INT	STRING	STRING	INT	INT	INT	INT	INT	INT
id	名字	代号	id1	id2	id3	id4	attr1	value1
4001	六道轮回	t_jibanex_name_4001	41	37			7	5
4002	生死对头	t_jibanex_name_4002	46	26	41		8	5
6001	迅疾如风	t_jibanex_name_6001	7	58			10	5
6002	自然守护	t_jibanex_name_6002	100	59	23		8	5
7001	疾风迅雷	t_jibanex_name_7001	8	46			10	5
7002	月影红莲	t_jibanex_name_7002	59	53	37		9	5
8001	疾风骤雨	t_jibanex_name_8001	6	26			10	5
8002	不死之身	t_jibanex_name_8002	66	59	46		8	5
23001	支配者	t_jibanex_name_23001	46	43			7	5
23002	晨曦之光	t_jibanex_name_23002	4	37	41		6	5
24001	风驰电掣	t_jibanex_name_24001	69	58			10	5
24002	究极正义	t_jibanex_name_24002	27	62	46		8	5
26001	绝对压制	t_jibanex_name_26001	37	23			7	5
26002	自然之力	t_jibanex_name_26002	41	43	4		9	5
27001	电光火石	t_jibanex_name_27001	49	43			10	5
27002	生存本能	t_jibanex_name_27002	35	42	26		6	5
35001	天外飞仙	t_jibanex_name_35001	69	26			10	5
35002	诡术女孩	t_jibanex_name_35002	57	49	43		8	5
36001	神之王座	t_jibanex_name_36001	67	23			10	5
36002	绝对领域	t_jibanex_name_36002	55	57	58		9	5
37001	雷霆之怒	t_jibanex_name_37001	43	4			7	5
37002	自然恩赐	t_jibanex_name_37002	26	46	58		9	5
41001	青山不改	t_jibanex_name_41001	58	26			7	5
41002	绿水长流	t_jibanex_name_41002	23	4	46		8	5
42001	影之力	t_jibanex_name_42001	36	43			10	5
42002	两小无猜	t_jibanex_name_42002	49	101	43		6	5
43001	心空妙有	t_jibanex_name_43001	4	46			10	5
43002	静若处子	t_jibanex_name_43002	41	23	37		6	5
46001	潜伏者	t_jibanex_name_46001	58	43			7	5
46002	和平卫士	t_jibanex_name_46002	37	4	26		8	5
49001	命运召唤	t_jibanex_name_49001	62	46			7	5
49002	白金之星	t_jibanex_name_49002	27	52	23		6	5
52001	最后的剑	t_jibanex_name_52001	66	46			7	5
52002	正义萝莉	t_jibanex_name_52002	53	69	41		6	5
53001	幸运星	t_jibanex_name_53001	42	37			10	5
53002	幻想少女	t_jibanex_name_53002	24	66	58		6	5
55001	女王的心	t_jibanex_name_55001	53	23			7	5
55002	红之魅惑	t_jibanex_name_55002	59	57	26		9	5
57001	死亡挽歌	t_jibanex_name_57001	59	23			7	5
57002	倾国倾城	t_jibanex_name_57002	36	53	46		6	5
58001	格斗术	t_jibanex_name_58001	43	26			10	5
58002	一刀流	t_jibanex_name_58002	41	46	23		9	5
59001	少女的救赎	t_jibanex_name_59001	6	37			7	5
59002	月蛇之力	t_jibanex_name_59002	7	8	26		9	5
62001	神之指引	t_jibanex_name_62001	101	4			7	5
62002	梦境传说	t_jibanex_name_62002	69	49	46		8	5
66001	奇迹现世	t_jibanex_name_66001	55	41			7	5
66002	少女的谢幕	t_jibanex_name_66002	24	27	23		8	5
67001	小伙伴	t_jibanex_name_67001	53	37			10	5
67002	达人秀	t_jibanex_name_67002	59	36	4		9	5
69001	主宰者	t_jibanex_name_69001	67	37			7	5
69002	命运预言	t_jibanex_name_69002	66	101	43		8	5
100001	领袖之力	t_jibanex_name_100001	69	58			7	5
100002	主角光环	t_jibanex_name_100002	24	35	43		8	5
101001	梦想少女	t_jibanex_name_101001	52	4			10	5
101002	操控之力	t_jibanex_name_101002	27	67	58		9	5
200001	逆袭	t_jibanex_name_200001	52	41			7	5
200002	铁霸王	t_jibanex_name_200002	24	57	26		6	5
200003	斯塔克	t_jibanex_name_200003	35	55	66	4	10	5
201001	杀戮之风	t_jibanex_name_201001	101	4			7	5
201002	冒险精神	t_jibanex_name_201002	24	55	46		9	5
201003	危险游戏	t_jibanex_name_201003	27	66	49	58	6	5
202001	制裁之力	t_jibanex_name_202001	35	43			7	5
202002	魔法克星	t_jibanex_name_202002	36	24	26		9	5
202003	王的庇护	t_jibanex_name_202003	42	52	69	23	6	5
203001	魅影迷踪	t_jibanex_name_203001	49	26			10	5
203002	枪炮玫瑰	t_jibanex_name_203002	52	36	41		6	5
203003	斩妖除魔	t_jibanex_name_203003	100	57	35	23	8	5
204001	战斗之心	t_jibanex_name_204001	55	46			7	5
204002	守护者	t_jibanex_name_204002	57	67	26		8	5
204003	复苏之风	t_jibanex_name_204003	62	27	52	58	6	5
205001	毁灭者	t_jibanex_name_205001	66	4			7	5
205002	开天辟地	t_jibanex_name_205002	67	42	41		6	5
205003	虔诚之心	t_jibanex_name_205003	69	53	62	37	9	5
206001	尊严之战	t_jibanex_name_206001	101	4			7	5
206002	温柔的心	t_jibanex_name_206002	27	62	43		6	5
206003	姐与妹	t_jibanex_name_206003	36	69	24	23	8	5
207001	指点江山	t_jibanex_name_207001	49	46			7	5
207002	刀之道	t_jibanex_name_207002	53	100	23		6	5
207003	惺惺相惜	t_jibanex_name_207003	57	36	101	4	10	5
208001	死亡使命	t_jibanex_name_208001	66	37			7	5
208002	坚不可摧	t_jibanex_name_208002	69	53	58		8	5
208003	红之蔷薇	t_jibanex_name_208003	24	27	36	46	10	5
209001	双刃剑	t_jibanex_name_209001	35	23			7	5
209002	天真无邪	t_jibanex_name_209002	42	66	37		6	5
209003	红白之境	t_jibanex_name_209003	52	55	49	26	10	5
211001	健步如飞	t_jibanex_name_211001	55	37			10	5
211002	空手白刃	t_jibanex_name_211002	62	42	46		8	5
211003	心如止水	t_jibanex_name_211003	100	27	53	41	6	5
212001	星光闪耀	t_jibanex_name_212001	101	4			7	5
212002	气定神闲	t_jibanex_name_212002	35	67	43		8	5
212003	决死意志	t_jibanex_name_212003	49	57	66	26	6	5
301001	惣流	t_jibanex_name_301001	212	101			7	5
301002	王牌驾驶员	t_jibanex_name_301002	211	52	42		9	5
301003	第二适格者	t_jibanex_name_301003	202	27	49	62	6	5
302001	梦幻之星	t_jibanex_name_302001	209	69			7	5
302002	人生导师	t_jibanex_name_302002	212	36	67		9	5
302003	莫塔维亚	t_jibanex_name_302003	203	42	53	101	6	5
303001	豪放不羁	t_jibanex_name_303001	207	49			10	5
303002	最强电击使	t_jibanex_name_303002	201	67	55		6	5
303003	常盘台的王牌	t_jibanex_name_303003	211	53	57	62	8	5
304001	狭间	t_jibanex_name_304001	205	55			7	5
304002	匡威战士	t_jibanex_name_304002	202	57	66		8	5
304003	请你为我唱首歌	t_jibanex_name_304003	212	101	27	36	6	5
305001	始祖	t_jibanex_name_305001	211	57			7	5
305002	灵魂容器	t_jibanex_name_305002	203	62	49		6	5
305003	第一适格者	t_jibanex_name_305003	201	101	52	27	9	5
306001	新的秩序	t_jibanex_name_306001	203	55			7	5
306002	龙族	t_jibanex_name_306002	204	62	59		6	5
306003	并肩作战	t_jibanex_name_306003	205	27	69	24	8	5
307001	楼观剑	t_jibanex_name_307001	201	24			7	5
307002	白楼剑	t_jibanex_name_307002	205	57	69		6	5
307003	西行樱花树	t_jibanex_name_307003	209	35	42	53	10	5
308001	从不彷徨	t_jibanex_name_308001	204	24			7	5
308002	从不困惑	t_jibanex_name_308002	207	66	49		8	5
308003	生死之境	t_jibanex_name_308003	206	42	67	35	10	5
309001	自由奔放	t_jibanex_name_309001	208	36			7	5
309002	五欲巫女	t_jibanex_name_309002	206	35	53		6	5
309003	八百万神的代言人	t_jibanex_name_309003	207	52	55	66	10	5
311001	必胜客披萨	t_jibanex_name_311001	206	24			10	5
311002	不老不死	t_jibanex_name_311002	209	101	49		8	5
311003	茜茜公主	t_jibanex_name_311003	208	35	36	55	6	5
312001	歌姬计划	t_jibanex_name_312001	202	35			7	5
312002	初音之日	t_jibanex_name_312002	208	66	24		8	5
312003	我来为你唱首歌	t_jibanex_name_312003	204	42	52	69	6	5
400001	荣誉	t_jibanex_name_400001	308	66			7	5
400002	理想乡	t_jibanex_name_400002	302	209	42		6	5
400003	若水庇护	t_jibanex_name_400003	301	212	205	8	10	5
401001	狂暴者	t_jibanex_name_401001	303	52			7	5
401002	破晓之光	t_jibanex_name_401002	306	201	57		6	5
401003	动如脱兔	t_jibanex_name_401003	307	204	208	62	10	5
402001	轻盈	t_jibanex_name_402001	305	62			7	5
402002	破茧	t_jibanex_name_402002	304	203	67		6	5
402003	表演	t_jibanex_name_402003	311	202	206	57	10	5
403001	王位	t_jibanex_name_403001	309	101			7	5
403002	白色骑士	t_jibanex_name_403002	312	211	35		6	5
403003	荣誉与骄傲	t_jibanex_name_403003	307	203	206	59	10	5
