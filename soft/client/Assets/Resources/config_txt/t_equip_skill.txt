INT	STRING	STRING	INT	INT	INT	INT	INT	STRING	STRING
id	name	代号	部位	精炼等级	type 1 加def1属性def2点 2 def1概率双倍攻击 3 受到攻击时，def1概率恢复def2%的生命 4 受到物理攻击时，def1概率反弹def2%伤害 5 受到魔法攻击时，def1概率反弹def2%伤害 6 def1概率破防def2% 7 受到普通攻击时，def1概率减少def2%伤害	def1	def2	描述	代号
1001	圣器强攻	t_equip_skill_name_1001	1	16	1	2	4000		t_equip_skill_desc_1001
1002	圣器能量	t_equip_skill_name_1002	1	21	1	22	1		t_equip_skill_desc_1002
1003	致命一击I	t_equip_skill_name_1003	1	26	2	2		每次攻击时，有{{n1}}%概率造成双倍伤害	t_equip_skill_desc_1003
1004	圣器强攻	t_equip_skill_name_1004	1	31	1	2	4000		t_equip_skill_desc_1004
1005	致命一击II	t_equip_skill_name_1005	1	35	2	5		造成双倍伤害概率提升至{{n1}}%	t_equip_skill_desc_1005
1006	圣器强攻	t_equip_skill_name_1006	1	39	1	2	4000		t_equip_skill_desc_1006
1007	致命一击III	t_equip_skill_name_1007	1	42	2	10		造成双倍伤害概率提升至{{n1}}%	t_equip_skill_desc_1007
1008	圣器强攻	t_equip_skill_name_1008	1	45	1	2	4000		t_equip_skill_desc_1008
1009	圣器强攻	t_equip_skill_name_1009	1	48	1	2	4000		t_equip_skill_desc_1009
1010	圣器强攻	t_equip_skill_name_1010	1	50	1	2	4000		t_equip_skill_desc_1010
2001	圣器强命	t_equip_skill_name_2001	2	16	1	1	32000		t_equip_skill_desc_2001
2002	圣器强命	t_equip_skill_name_2002	2	21	1	1	32000		t_equip_skill_desc_2002
2003	毅力抵抗I	t_equip_skill_name_2003	2	26	3	30	2	每次受到攻击时，有{{n1}}%概率回复{{n2}}%的生命	t_equip_skill_desc_2003
2004	圣器强命	t_equip_skill_name_2004	2	31	1	1	32000		t_equip_skill_desc_2004
2005	毅力抵抗II	t_equip_skill_name_2005	2	35	3	30	5	回复生命比例提升至{{n2}}%	t_equip_skill_desc_2005
2006	圣器强命	t_equip_skill_name_2006	2	39	1	1	32000		t_equip_skill_desc_2006
2007	毅力抵抗III	t_equip_skill_name_2007	2	42	3	30	10	回复生命比例提升至{{n2}}%	t_equip_skill_desc_2007
2008	圣器强命	t_equip_skill_name_2008	2	45	1	1	32000		t_equip_skill_desc_2008
2009	圣器强命	t_equip_skill_name_2009	2	48	1	1	32000		t_equip_skill_desc_2009
2010	圣器强命	t_equip_skill_name_2010	2	50	1	1	32000		t_equip_skill_desc_2010
3001	圣器物防	t_equip_skill_name_3001	3	16	1	3	3200		t_equip_skill_desc_3001
3002	圣器物防	t_equip_skill_name_3002	3	21	1	3	3200		t_equip_skill_desc_3002
3003	物理反弹I	t_equip_skill_name_3003	3	26	4	50	5	每次受到物理攻击时，有{{n1}}%概率反弹{{n2}}%的伤害	t_equip_skill_desc_3003
3004	圣器物防	t_equip_skill_name_3004	3	31	1	3	3200		t_equip_skill_desc_3004
3005	物理反弹II	t_equip_skill_name_3005	3	35	4	50	10	反弹伤害比例提升至{{n2}}%	t_equip_skill_desc_3005
3006	圣器物防	t_equip_skill_name_3006	3	39	1	3	3200		t_equip_skill_desc_3006
3007	物理反弹III	t_equip_skill_name_3007	3	42	4	50	20	反弹伤害比例提升至{{n2}}%	t_equip_skill_desc_3007
3008	圣器物防	t_equip_skill_name_3008	3	45	1	3	3200		t_equip_skill_desc_3008
3009	圣器物防	t_equip_skill_name_3009	3	48	1	3	3200		t_equip_skill_desc_3009
3010	圣器物防	t_equip_skill_name_3010	3	50	1	3	3200		t_equip_skill_desc_3010
4001	圣器魔防	t_equip_skill_name_4001	4	16	1	4	3200		t_equip_skill_desc_4001
4002	圣器魔防	t_equip_skill_name_4002	4	21	1	4	3200		t_equip_skill_desc_4002
4003	魔法反弹I	t_equip_skill_name_4003	4	26	5	50	5	每次受到魔法攻击时，有{{n1}}%概率反弹{{n2}}%的伤害	t_equip_skill_desc_4003
4004	圣器魔防	t_equip_skill_name_4004	4	31	1	4	3200		t_equip_skill_desc_4004
4005	魔法反弹II	t_equip_skill_name_4005	4	35	5	50	10	反弹伤害比例提升至{{n2}}%	t_equip_skill_desc_4005
4006	圣器魔防	t_equip_skill_name_4006	4	39	1	4	3200		t_equip_skill_desc_4006
4007	魔法反弹III	t_equip_skill_name_4007	4	42	5	50	20	反弹伤害比例提升至{{n2}}%	t_equip_skill_desc_4007
4008	圣器魔防	t_equip_skill_name_4008	4	45	1	4	3200		t_equip_skill_desc_4008
4009	圣器魔防	t_equip_skill_name_4009	4	48	1	4	3200		t_equip_skill_desc_4009
4010	圣器魔防	t_equip_skill_name_4010	4	50	1	4	3200		t_equip_skill_desc_4010
5001	圣器强攻	t_equip_skill_name_5001	5	5	1	2	3000		t_equip_skill_desc_5001
5002	圣器强攻	t_equip_skill_name_5002	5	8	1	2	3000		t_equip_skill_desc_5002
5003	圣器强攻	t_equip_skill_name_5003	5	10	1	2	3000		t_equip_skill_desc_5003
5004	破甲一击I	t_equip_skill_name_5004	5	12	6	10	50	每次攻击时，有{{n1}}%概率无视{{n2}}%的防御	t_equip_skill_desc_5004
5005	圣器强攻	t_equip_skill_name_5005	5	14	1	2	3000		t_equip_skill_desc_5005
5006	破甲一击II	t_equip_skill_name_5006	5	16	6	20	50	无视防御概率提升至{{n1}}%	t_equip_skill_desc_5006
5007	圣器强攻	t_equip_skill_name_5007	5	18	1	2	3000		t_equip_skill_desc_5007
5008	破甲一击III	t_equip_skill_name_5008	5	20	6	30	50	无视防御概率提升至{{n1}}%	t_equip_skill_desc_5008
6001	圣器强命	t_equip_skill_name_6001	6	5	1	1	30000		t_equip_skill_desc_6001
6002	圣器强命	t_equip_skill_name_6002	6	8	1	1	30000		t_equip_skill_desc_6002
6003	圣器强命	t_equip_skill_name_6003	6	10	1	1	30000		t_equip_skill_desc_6003
6004	超级护甲I	t_equip_skill_name_6004	6	12	7	50	10	每次受到普通攻击时，有{{n1}}%概率减少{{n2}}%的伤害	t_equip_skill_desc_6004
6005	圣器强命	t_equip_skill_name_6005	6	14	1	1	30000		t_equip_skill_desc_6005
6006	超级护甲II	t_equip_skill_name_6006	6	16	7	50	20	减少伤害比例提升至{{n2}}%	t_equip_skill_desc_6006
6007	圣器强命	t_equip_skill_name_6007	6	18	1	1	30000		t_equip_skill_desc_6007
6008	超级护甲III	t_equip_skill_name_6008	6	20	7	50	40	减少伤害比例提升至{{n2}}%	t_equip_skill_desc_6008
