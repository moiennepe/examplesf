INT	STRING	INT	STRING	STRING	FLOAT
id	名字	属性位置	desc	代号	战斗力
1	t_value_name_1	1	生命 +{n}	t_value_desc_1	0.25
2	t_value_name_2	2	攻击 +{n}	t_value_desc_2	2
3	t_value_name_3	3	物防 +{n}	t_value_desc_3	5
4	t_value_name_4	4	魔防 +{n}	t_value_desc_4	5
5	t_value_name_5	18	先攻 +{n}	t_value_desc_5	5
6	t_value_name_6	5	生命 +{n}%	t_value_desc_6	
7	t_value_name_7	6	攻击 +{n}%	t_value_desc_7	
8	t_value_name_8	7	物防 +{n}%	t_value_desc_8	
9	t_value_name_9	8	魔防 +{n}%	t_value_desc_9	
10	t_value_name_10	19	先攻 +{n}%	t_value_desc_10	
11	t_value_name_11	11	暴击 +{n}%	t_value_desc_11	0.003
12	t_value_name_12	12	格挡 +{n}%	t_value_desc_12	0.003
13	t_value_name_13	15	晕眩	t_value_desc_13	
14	t_value_name_14	9	物免 +{n}%	t_value_desc_14	0.003
15	t_value_name_15	10	魔免 +{n}%	t_value_desc_15	0.003
16	t_value_name_16		命中 +{n}%	t_value_desc_16	0.003
17	t_value_name_17		闪避 +{n}%	t_value_desc_17	0.003
18	t_value_name_18		抗暴 +{n}%	t_value_desc_18	0.003
19	t_value_name_19		穿透 +{n}%	t_value_desc_19	0.003
20	t_value_name_20		增伤 +{n}%	t_value_desc_20	0.003
21	t_value_name_21		减伤 +{n}%	t_value_desc_21	0.003
22	t_value_name_22		基础能量 +{n}	t_value_desc_22	0.05
23	t_value_name_23		灭防 +{n}%	t_value_desc_23	0.0003
24	t_value_name_24		灭物 +{n}%	t_value_desc_24	0.0003
25	t_value_name_25		灭魔 +{n}%	t_value_desc_25	0.0003
26	t_value_name_26		抗防 +{n}%	t_value_desc_26	0.0003
27	t_value_name_27		抗物 +{n}%	t_value_desc_27	0.0003
28	t_value_name_28		抗魔 +{n}%	t_value_desc_28	0.0003
29	t_value_name_29		防御 +{n}	t_value_desc_29	
30	t_value_name_30		防御 +{n}%	t_value_desc_30	
31	t_value_name_31		物攻 +{n}	t_value_desc_31	
32	t_value_name_32		魔攻 +{n}	t_value_desc_32	
33	t_value_name_33		物攻 +{n}%	t_value_desc_33	
34	t_value_name_34		魔攻 +{n}%	t_value_desc_34	
35	t_value_name_35		PVP增伤 +{n}%	t_value_desc_35	0.0003
36	t_value_name_36		PVP减伤 +{n}%	t_value_desc_36	0.0003
37	t_value_name_37		全属性 +{n}	t_value_desc_37	
38	t_value_name_38		全属性 +{n}%	t_value_desc_38	
