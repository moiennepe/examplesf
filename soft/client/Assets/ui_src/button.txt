{"frames": {

"an_tongyong01a.png":
{
	"frame": {"x":2,"y":245,"w":120,"h":53},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":120,"h":53},
	"sourceSize": {"w":120,"h":55}
},
"an_tongyong01b.png":
{
	"frame": {"x":124,"y":240,"w":120,"h":53},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":120,"h":53},
	"sourceSize": {"w":120,"h":55}
},
"an_tongyong01c.png":
{
	"frame": {"x":2,"y":190,"w":120,"h":53},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":120,"h":53},
	"sourceSize": {"w":120,"h":55}
},
"an_tongyong01d.png":
{
	"frame": {"x":130,"y":185,"w":120,"h":53},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":120,"h":53},
	"sourceSize": {"w":120,"h":55}
},
"an_tongyong01e.png":
{
	"frame": {"x":130,"y":130,"w":120,"h":53},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":120,"h":53},
	"sourceSize": {"w":120,"h":55}
},
"atyan_hs01.png":
{
	"frame": {"x":346,"y":121,"w":137,"h":57},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":137,"h":57},
	"sourceSize": {"w":137,"h":57}
},
"atyan_ls01.png":
{
	"frame": {"x":366,"y":62,"w":137,"h":57},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":137,"h":57},
	"sourceSize": {"w":137,"h":57}
},
"bbfy_glzt001.png":
{
	"frame": {"x":366,"y":2,"w":138,"h":58},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":138,"h":58},
	"sourceSize": {"w":138,"h":58}
},
"bbfy_ptzt001.png":
{
	"frame": {"x":2,"y":130,"w":126,"h":58},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":0,"w":126,"h":58},
	"sourceSize": {"w":138,"h":58}
},
"bz_ironanniu01.png":
{
	"frame": {"x":282,"y":241,"w":34,"h":85},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":34,"h":85},
	"sourceSize": {"w":34,"h":85}
},
"bz_ironanniu01_ft.png":
{
	"frame": {"x":246,"y":241,"w":34,"h":85},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":34,"h":85},
	"sourceSize": {"w":34,"h":85}
},
"bz_ironanniu01a.png":
{
	"frame": {"x":454,"y":267,"w":34,"h":85},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":34,"h":85},
	"sourceSize": {"w":34,"h":85}
},
"bz_ironanniu01b.png":
{
	"frame": {"x":454,"y":180,"w":34,"h":85},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":34,"h":85},
	"sourceSize": {"w":34,"h":85}
},
"bz_ironframe02_b01.png":
{
	"frame": {"x":2,"y":407,"w":110,"h":45},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":4,"w":110,"h":45},
	"sourceSize": {"w":110,"h":49}
},
"bz_ironframe02_b02.png":
{
	"frame": {"x":2,"y":360,"w":110,"h":45},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":110,"h":45},
	"sourceSize": {"w":110,"h":49}
},
"dx_rq_dp01_button_01.png":
{
	"frame": {"x":210,"y":389,"w":100,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":100,"h":32},
	"sourceSize": {"w":100,"h":32}
},
"dx_rq_dp01_button_02.png":
{
	"frame": {"x":410,"y":385,"w":96,"h":28},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":96,"h":28},
	"sourceSize": {"w":100,"h":32}
},
"dx_rq_dp01_button_02_1.png":
{
	"frame": {"x":312,"y":385,"w":96,"h":28},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":96,"h":28},
	"sourceSize": {"w":100,"h":32}
},
"dx_rq_dp01_button_03.png":
{
	"frame": {"x":388,"y":355,"w":96,"h":28},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":96,"h":28},
	"sourceSize": {"w":100,"h":32}
},
"dx_rq_dp01_button_03_1.png":
{
	"frame": {"x":290,"y":423,"w":96,"h":28},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":96,"h":28},
	"sourceSize": {"w":100,"h":32}
},
"dx_rq_dp02_button_01.png":
{
	"frame": {"x":210,"y":355,"w":100,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":100,"h":32},
	"sourceSize": {"w":100,"h":32}
},
"dx_rq_dp03_button_01.png":
{
	"frame": {"x":194,"y":466,"w":100,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":100,"h":32},
	"sourceSize": {"w":100,"h":32}
},
"jgmf_tbbm.png":
{
	"frame": {"x":252,"y":154,"w":82,"h":85},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":82,"h":85},
	"sourceSize": {"w":82,"h":85}
},
"szjm_jsxan.png":
{
	"frame": {"x":194,"y":429,"w":94,"h":35},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":94,"h":35},
	"sourceSize": {"w":94,"h":35}
},
"szjm_tyxan_ft.png":
{
	"frame": {"x":114,"y":392,"w":94,"h":35},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":94,"h":35},
	"sourceSize": {"w":94,"h":35}
},
"szjm_yzbxan.png":
{
	"frame": {"x":114,"y":355,"w":94,"h":35},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":94,"h":35},
	"sourceSize": {"w":94,"h":35}
},
"szjm_zbxan.png":
{
	"frame": {"x":98,"y":454,"w":94,"h":35},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":94,"h":35},
	"sourceSize": {"w":94,"h":35}
},
"szjm_zsxan.png":
{
	"frame": {"x":2,"y":454,"w":94,"h":35},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":94,"h":35},
	"sourceSize": {"w":94,"h":35}
},
"xiean_lt001a.png":
{
	"frame": {"x":156,"y":67,"w":152,"h":61},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":0,"w":152,"h":61},
	"sourceSize": {"w":160,"h":65}
},
"xiean_lt001b.png":
{
	"frame": {"x":2,"y":67,"w":152,"h":61},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":0,"w":152,"h":61},
	"sourceSize": {"w":160,"h":65}
},
"xsdan_aan001.png":
{
	"frame": {"x":318,"y":295,"w":110,"h":58},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":110,"h":58},
	"sourceSize": {"w":110,"h":58}
},
"xsdan_aan002.png":
{
	"frame": {"x":2,"y":300,"w":110,"h":58},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":110,"h":58},
	"sourceSize": {"w":110,"h":58}
},
"xsdan_aan003f.png":
{
	"frame": {"x":124,"y":295,"w":110,"h":58},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":110,"h":58},
	"sourceSize": {"w":110,"h":58}
},
"yjxx_an.png":
{
	"frame": {"x":310,"y":67,"w":34,"h":85},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":34,"h":85},
	"sourceSize": {"w":34,"h":85}
},
"zbcq_an_xin.png":
{
	"frame": {"x":184,"y":2,"w":180,"h":63},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":180,"h":63},
	"sourceSize": {"w":180,"h":63}
},
"zbcq_an_xin002.png":
{
	"frame": {"x":336,"y":180,"w":116,"h":113},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":1,"w":116,"h":113},
	"sourceSize": {"w":118,"h":115}
},
"zbcq_an_xin01.png":
{
	"frame": {"x":2,"y":2,"w":180,"h":63},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":180,"h":63},
	"sourceSize": {"w":180,"h":63}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "button.png",
	"format": "RGBA8888",
	"size": {"w":512,"h":512},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:1b4bd92ad404c5e60672f6656523247c$"
}
}
