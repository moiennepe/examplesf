{"frames": {

"ch_db_001.png":
{
	"frame": {"x":148,"y":217,"w":144,"h":21},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":7,"w":144,"h":21},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_001a.png":
{
	"frame": {"x":294,"y":214,"w":144,"h":21},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":7,"w":144,"h":21},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_002.png":
{
	"frame": {"x":2,"y":204,"w":144,"h":21},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":7,"w":144,"h":21},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_002a.png":
{
	"frame": {"x":148,"y":194,"w":144,"h":21},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":7,"w":144,"h":21},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_003.png":
{
	"frame": {"x":2,"y":256,"w":142,"h":27},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":3,"w":142,"h":27},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_003a.png":
{
	"frame": {"x":146,"y":240,"w":142,"h":27},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":3,"w":142,"h":27},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_004.png":
{
	"frame": {"x":294,"y":237,"w":142,"h":27},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":3,"w":142,"h":27},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_004a.png":
{
	"frame": {"x":2,"y":227,"w":142,"h":27},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":3,"w":142,"h":27},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_005.png":
{
	"frame": {"x":146,"y":269,"w":138,"h":31},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":0,"w":138,"h":31},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_005a.png":
{
	"frame": {"x":290,"y":266,"w":138,"h":31},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":0,"w":138,"h":31},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_006.png":
{
	"frame": {"x":296,"y":179,"w":144,"h":33},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":3,"w":144,"h":33},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_006a.png":
{
	"frame": {"x":2,"y":169,"w":144,"h":33},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":3,"w":144,"h":33},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_007.png":
{
	"frame": {"x":154,"y":89,"w":148,"h":35},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":148,"h":35},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_007a.png":
{
	"frame": {"x":306,"y":68,"w":148,"h":35},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":148,"h":35},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_008.png":
{
	"frame": {"x":150,"y":159,"w":144,"h":33},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":2,"w":144,"h":33},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_008a.png":
{
	"frame": {"x":298,"y":144,"w":144,"h":33},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":2,"w":144,"h":33},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_009.png":
{
	"frame": {"x":304,"y":105,"w":146,"h":37},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":0,"w":146,"h":37},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_009a.png":
{
	"frame": {"x":2,"y":97,"w":146,"h":37},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":0,"w":146,"h":37},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_010.png":
{
	"frame": {"x":170,"y":23,"w":152,"h":31},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":4,"w":152,"h":31},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_010a.png":
{
	"frame": {"x":330,"y":2,"w":152,"h":31},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":4,"w":152,"h":31},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_011.png":
{
	"frame": {"x":2,"y":136,"w":146,"h":31},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":2,"w":146,"h":31},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_011a.png":
{
	"frame": {"x":150,"y":126,"w":146,"h":31},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":2,"w":146,"h":31},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_012.png":
{
	"frame": {"x":2,"y":64,"w":150,"h":31},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":2,"w":150,"h":31},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_012a.png":
{
	"frame": {"x":154,"y":56,"w":150,"h":31},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":2,"w":150,"h":31},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_013.png":
{
	"frame": {"x":324,"y":35,"w":150,"h":31},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":2,"w":150,"h":31},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_013a.png":
{
	"frame": {"x":2,"y":31,"w":150,"h":31},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":2,"w":150,"h":31},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_014.png":
{
	"frame": {"x":106,"y":302,"w":102,"h":19},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":25,"y":8,"w":102,"h":19},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_014a.png":
{
	"frame": {"x":390,"y":299,"w":102,"h":19},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":25,"y":8,"w":102,"h":19},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_015.png":
{
	"frame": {"x":286,"y":299,"w":102,"h":19},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":25,"y":8,"w":102,"h":19},
	"sourceSize": {"w":152,"h":37}
},
"ch_db_015a.png":
{
	"frame": {"x":2,"y":285,"w":102,"h":19},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":25,"y":8,"w":102,"h":19},
	"sourceSize": {"w":152,"h":37}
},
"ch_icon_01.png":
{
	"frame": {"x":2,"y":2,"w":166,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":166,"h":27},
	"sourceSize": {"w":166,"h":27}
},
"ch_icon_02.png":
{
	"frame": {"x":170,"y":2,"w":158,"h":19},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":4,"w":158,"h":19},
	"sourceSize": {"w":166,"h":27}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "chenghao_gui.png",
	"format": "RGBA8888",
	"size": {"w":512,"h":512},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:343ea9598aae5e1a0dbf6b2340125929$"
}
}
