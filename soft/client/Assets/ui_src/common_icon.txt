{"frames": {

"ckdj_gojzmx.png":
{
	"frame": {"x":484,"y":158,"w":26,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":0,"w":26,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"ckdj_jin.png":
{
	"frame": {"x":362,"y":242,"w":28,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":3,"w":28,"h":26},
	"sourceSize": {"w":30,"h":30}
},
"ckdj_yqzomx.png":
{
	"frame": {"x":182,"y":474,"w":30,"h":30},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":30,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"cwhb_icon01.png":
{
	"frame": {"x":380,"y":396,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"cwhb_icon02.png":
{
	"frame": {"x":362,"y":182,"w":30,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":3,"w":30,"h":26},
	"sourceSize": {"w":30,"h":30}
},
"dj_vip.png":
{
	"frame": {"x":290,"y":380,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"gold_icon.png":
{
	"frame": {"x":362,"y":270,"w":26,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":2,"w":26,"h":26},
	"sourceSize": {"w":30,"h":30}
},
"gold_icon_80.png":
{
	"frame": {"x":200,"y":380,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"gx_tb03.png":
{
	"frame": {"x":362,"y":324,"w":24,"h":24},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":10,"y":10,"w":24,"h":24},
	"sourceSize": {"w":44,"h":44}
},
"gzs_xtb.png":
{
	"frame": {"x":482,"y":297,"w":24,"h":24},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":3,"w":24,"h":24},
	"sourceSize": {"w":30,"h":30}
},
"hshbzlx_icon.png":
{
	"frame": {"x":150,"y":474,"w":30,"h":30},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":30,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"hszbzlx_icon.png":
{
	"frame": {"x":458,"y":98,"w":30,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":2,"w":30,"h":26},
	"sourceSize": {"w":30,"h":30}
},
"hyjjd_icon.png":
{
	"frame": {"x":390,"y":306,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"hyjjx_icon.png":
{
	"frame": {"x":362,"y":350,"w":22,"h":28},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":1,"w":22,"h":28},
	"sourceSize": {"w":30,"h":30}
},
"icon_bjd.png":
{
	"frame": {"x":272,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_bjx.png":
{
	"frame": {"x":484,"y":126,"w":26,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":26,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"icon_fpjf.png":
{
	"frame": {"x":118,"y":474,"w":30,"h":30},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":30,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"icon_jljsd001.png":
{
	"frame": {"x":392,"y":216,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_jljsx001.png":
{
	"frame": {"x":182,"y":152,"w":30,"h":28},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":30,"h":28},
	"sourceSize": {"w":30,"h":30}
},
"icon_szjsd.png":
{
	"frame": {"x":272,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_szjsx.png":
{
	"frame": {"x":182,"y":122,"w":30,"h":28},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":30,"h":28},
	"sourceSize": {"w":30,"h":30}
},
"jjc_dsb.png":
{
	"frame": {"x":182,"y":218,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jjc_dss.png":
{
	"frame": {"x":478,"y":66,"w":30,"h":30},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":30,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"jjcds.png":
{
	"frame": {"x":478,"y":34,"w":30,"h":30},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":30,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"jjcds_80.png":
{
	"frame": {"x":394,"y":126,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jltb_icon.png":
{
	"frame": {"x":304,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jltbx_icon.png":
{
	"frame": {"x":92,"y":32,"w":24,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":1,"w":24,"h":26},
	"sourceSize": {"w":30,"h":30}
},
"jlysb_icon.png":
{
	"frame": {"x":214,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jlysbx_icon.png":
{
	"frame": {"x":362,"y":210,"w":28,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":28,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"jlystb_icon.png":
{
	"frame": {"x":110,"y":384,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jlystbx_icon.png":
{
	"frame": {"x":242,"y":182,"w":28,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":28,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"jnsjsd_001.png":
{
	"frame": {"x":92,"y":294,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jnsjsx_001.png":
{
	"frame": {"x":92,"y":2,"w":24,"h":28},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":1,"w":24,"h":28},
	"sourceSize": {"w":30,"h":30}
},
"jp.png":
{
	"frame": {"x":480,"y":379,"w":22,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":1,"w":22,"h":26},
	"sourceSize": {"w":30,"h":30}
},
"jugx_dtb001.png":
{
	"frame": {"x":92,"y":204,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jugx_dtb002.png":
{
	"frame": {"x":86,"y":484,"w":30,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":3,"w":30,"h":26},
	"sourceSize": {"w":30,"h":30}
},
"lock.png":
{
	"frame": {"x":490,"y":98,"w":20,"h":24},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":3,"w":20,"h":24},
	"sourceSize": {"w":30,"h":30}
},
"lrjzd_icon.png":
{
	"frame": {"x":92,"y":114,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"lrjzx_icon.png":
{
	"frame": {"x":182,"y":92,"w":30,"h":28},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":30,"h":28},
	"sourceSize": {"w":30,"h":30}
},
"mwjz.png":
{
	"frame": {"x":478,"y":2,"w":30,"h":30},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":30,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"mwjz_80.png":
{
	"frame": {"x":388,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"mwyqkx_001.png":
{
	"frame": {"x":212,"y":182,"w":28,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":28,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"nmwjz.png":
{
	"frame": {"x":480,"y":351,"w":22,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":1,"w":22,"h":26},
	"sourceSize": {"w":30,"h":30}
},
"nmwjz_80.png":
{
	"frame": {"x":298,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"sp_gojzm.png":
{
	"frame": {"x":208,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"spzdl_xtb-fb.png":
{
	"frame": {"x":394,"y":92,"w":62,"h":22},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":6,"w":62,"h":22},
	"sourceSize": {"w":70,"h":30}
},
"spzdl_xtb.png":
{
	"frame": {"x":482,"y":271,"w":24,"h":24},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":2,"w":24,"h":24},
	"sourceSize": {"w":30,"h":30}
},
"spzdl_xtb_overseas.png":
{
	"frame": {"x":482,"y":218,"w":26,"h":24},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":3,"w":26,"h":24},
	"sourceSize": {"w":30,"h":30}
},
"tb_sjjt001.png":
{
	"frame": {"x":40,"y":452,"w":36,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":36,"h":36},
	"sourceSize": {"w":36,"h":36}
},
"tb_sjjt002.png":
{
	"frame": {"x":2,"y":452,"w":36,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":36,"h":36},
	"sourceSize": {"w":36,"h":36}
},
"tl.png":
{
	"frame": {"x":480,"y":323,"w":22,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":1,"w":22,"h":26},
	"sourceSize": {"w":30,"h":30}
},
"tl_80.png":
{
	"frame": {"x":118,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xp_icon.png":
{
	"frame": {"x":2,"y":490,"w":30,"h":20},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":4,"w":30,"h":20},
	"sourceSize": {"w":30,"h":30}
},
"xp_icon_1.png":
{
	"frame": {"x":34,"y":490,"w":30,"h":18},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":4,"w":30,"h":18},
	"sourceSize": {"w":30,"h":30}
},
"xp_icon_80.png":
{
	"frame": {"x":2,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xstar_001.png":
{
	"frame": {"x":138,"y":92,"w":18,"h":18},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":18,"h":18},
	"sourceSize": {"w":18,"h":18}
},
"xstar_001d.png":
{
	"frame": {"x":482,"y":244,"w":24,"h":25},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":24,"h":25},
	"sourceSize": {"w":24,"h":25}
},
"xstar_002.png":
{
	"frame": {"x":118,"y":92,"w":18,"h":18},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":18,"h":18},
	"sourceSize": {"w":18,"h":18}
},
"xstar_002d.png":
{
	"frame": {"x":92,"y":87,"w":24,"h":25},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":24,"h":25},
	"sourceSize": {"w":24,"h":25}
},
"xstar_003.png":
{
	"frame": {"x":66,"y":490,"w":18,"h":18},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":18,"h":18},
	"sourceSize": {"w":18,"h":18}
},
"xstar_003d.png":
{
	"frame": {"x":92,"y":60,"w":24,"h":25},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":24,"h":25},
	"sourceSize": {"w":24,"h":25}
},
"xtb_gxztb00.png":
{
	"frame": {"x":78,"y":452,"w":30,"h":30},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":30,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"xtb_gxztb01.png":
{
	"frame": {"x":2,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xydd_icon.png":
{
	"frame": {"x":2,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xydx_icon.png":
{
	"frame": {"x":182,"y":182,"w":28,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":28,"h":30},
	"sourceSize": {"w":30,"h":30}
},
"yl_30s.png":
{
	"frame": {"x":484,"y":190,"w":26,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":2,"w":26,"h":26},
	"sourceSize": {"w":30,"h":30}
},
"yl_80sa01.png":
{
	"frame": {"x":2,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zs.png":
{
	"frame": {"x":362,"y":298,"w":26,"h":24},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":4,"w":26,"h":24},
	"sourceSize": {"w":30,"h":30}
},
"zs_80.png":
{
	"frame": {"x":2,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "common_icon.png",
	"format": "RGBA8888",
	"size": {"w":512,"h":512},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:99174d26da1df6d008849d9510d1a8b8$"
}
}
