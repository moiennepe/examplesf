{"frames": {

"gq_000.png":
{
	"frame": {"x":266,"y":230,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_001.png":
{
	"frame": {"x":200,"y":230,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_002.png":
{
	"frame": {"x":926,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_003.png":
{
	"frame": {"x":860,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_004.png":
{
	"frame": {"x":794,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_005.png":
{
	"frame": {"x":728,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_006.png":
{
	"frame": {"x":662,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_007.png":
{
	"frame": {"x":596,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_008.png":
{
	"frame": {"x":530,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_009.png":
{
	"frame": {"x":464,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_010.png":
{
	"frame": {"x":398,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_011.png":
{
	"frame": {"x":332,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_012.png":
{
	"frame": {"x":266,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_013.png":
{
	"frame": {"x":200,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_014.png":
{
	"frame": {"x":134,"y":952,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_015.png":
{
	"frame": {"x":134,"y":914,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_016.png":
{
	"frame": {"x":134,"y":876,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_017.png":
{
	"frame": {"x":134,"y":838,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_018.png":
{
	"frame": {"x":134,"y":800,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_019.png":
{
	"frame": {"x":134,"y":762,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_020.png":
{
	"frame": {"x":134,"y":724,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_021.png":
{
	"frame": {"x":134,"y":686,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_022.png":
{
	"frame": {"x":134,"y":648,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_023.png":
{
	"frame": {"x":134,"y":610,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_024.png":
{
	"frame": {"x":134,"y":572,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_025.png":
{
	"frame": {"x":134,"y":534,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_026.png":
{
	"frame": {"x":134,"y":496,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_027.png":
{
	"frame": {"x":134,"y":458,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_028.png":
{
	"frame": {"x":134,"y":420,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_029.png":
{
	"frame": {"x":134,"y":382,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_030.png":
{
	"frame": {"x":134,"y":344,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_031.png":
{
	"frame": {"x":134,"y":306,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_032.png":
{
	"frame": {"x":134,"y":268,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_033.png":
{
	"frame": {"x":134,"y":230,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_034.png":
{
	"frame": {"x":134,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_035.png":
{
	"frame": {"x":926,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_036.png":
{
	"frame": {"x":860,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_037.png":
{
	"frame": {"x":794,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_038.png":
{
	"frame": {"x":728,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_039.png":
{
	"frame": {"x":662,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_040.png":
{
	"frame": {"x":596,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_041.png":
{
	"frame": {"x":530,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_042.png":
{
	"frame": {"x":464,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_043.png":
{
	"frame": {"x":398,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_044.png":
{
	"frame": {"x":332,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_045.png":
{
	"frame": {"x":266,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_046.png":
{
	"frame": {"x":200,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_047.png":
{
	"frame": {"x":134,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_048.png":
{
	"frame": {"x":68,"y":952,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_049.png":
{
	"frame": {"x":68,"y":914,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_050.png":
{
	"frame": {"x":68,"y":876,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_051.png":
{
	"frame": {"x":68,"y":838,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_052.png":
{
	"frame": {"x":68,"y":800,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_053.png":
{
	"frame": {"x":68,"y":762,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_054.png":
{
	"frame": {"x":68,"y":724,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_055.png":
{
	"frame": {"x":68,"y":686,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_056.png":
{
	"frame": {"x":68,"y":648,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_057.png":
{
	"frame": {"x":68,"y":610,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_058.png":
{
	"frame": {"x":68,"y":572,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_059.png":
{
	"frame": {"x":68,"y":534,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_060.png":
{
	"frame": {"x":68,"y":496,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_061.png":
{
	"frame": {"x":68,"y":458,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_062.png":
{
	"frame": {"x":68,"y":420,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_063.png":
{
	"frame": {"x":68,"y":382,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_064.png":
{
	"frame": {"x":68,"y":344,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_065.png":
{
	"frame": {"x":68,"y":306,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_066.png":
{
	"frame": {"x":68,"y":268,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_067.png":
{
	"frame": {"x":68,"y":230,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_068.png":
{
	"frame": {"x":68,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_069.png":
{
	"frame": {"x":68,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_070.png":
{
	"frame": {"x":926,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_071.png":
{
	"frame": {"x":860,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_072.png":
{
	"frame": {"x":794,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_073.png":
{
	"frame": {"x":728,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_074.png":
{
	"frame": {"x":662,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_075.png":
{
	"frame": {"x":596,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_076.png":
{
	"frame": {"x":530,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_077.png":
{
	"frame": {"x":464,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_078.png":
{
	"frame": {"x":398,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_079.png":
{
	"frame": {"x":332,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_080.png":
{
	"frame": {"x":266,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_081.png":
{
	"frame": {"x":200,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_082.png":
{
	"frame": {"x":134,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_083.png":
{
	"frame": {"x":68,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_084.png":
{
	"frame": {"x":926,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_085.png":
{
	"frame": {"x":860,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_086.png":
{
	"frame": {"x":794,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_087.png":
{
	"frame": {"x":728,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_088.png":
{
	"frame": {"x":662,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_089.png":
{
	"frame": {"x":596,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_090.png":
{
	"frame": {"x":530,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_091.png":
{
	"frame": {"x":464,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_092.png":
{
	"frame": {"x":398,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_093.png":
{
	"frame": {"x":332,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_094.png":
{
	"frame": {"x":266,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_095.png":
{
	"frame": {"x":200,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_096.png":
{
	"frame": {"x":134,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_097.png":
{
	"frame": {"x":68,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_098.png":
{
	"frame": {"x":2,"y":952,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_099.png":
{
	"frame": {"x":2,"y":914,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_100.png":
{
	"frame": {"x":2,"y":876,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_101.png":
{
	"frame": {"x":2,"y":838,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_102.png":
{
	"frame": {"x":2,"y":800,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_103.png":
{
	"frame": {"x":2,"y":762,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_104.png":
{
	"frame": {"x":2,"y":724,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_105.png":
{
	"frame": {"x":2,"y":686,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_106.png":
{
	"frame": {"x":2,"y":648,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_107.png":
{
	"frame": {"x":2,"y":610,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_108.png":
{
	"frame": {"x":2,"y":572,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_109.png":
{
	"frame": {"x":2,"y":534,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_110.png":
{
	"frame": {"x":2,"y":496,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_111.png":
{
	"frame": {"x":2,"y":458,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_112.png":
{
	"frame": {"x":2,"y":420,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_113.png":
{
	"frame": {"x":2,"y":382,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_114.png":
{
	"frame": {"x":2,"y":344,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_115.png":
{
	"frame": {"x":2,"y":306,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_116.png":
{
	"frame": {"x":2,"y":268,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_117.png":
{
	"frame": {"x":2,"y":230,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_118.png":
{
	"frame": {"x":2,"y":192,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_119.png":
{
	"frame": {"x":2,"y":154,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_120.png":
{
	"frame": {"x":2,"y":116,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_121.png":
{
	"frame": {"x":2,"y":78,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_122.png":
{
	"frame": {"x":926,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_123.png":
{
	"frame": {"x":860,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_124.png":
{
	"frame": {"x":794,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_125.png":
{
	"frame": {"x":728,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_126.png":
{
	"frame": {"x":662,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_127.png":
{
	"frame": {"x":596,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_128.png":
{
	"frame": {"x":530,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_129.png":
{
	"frame": {"x":464,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_130.png":
{
	"frame": {"x":398,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_131.png":
{
	"frame": {"x":332,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_132.png":
{
	"frame": {"x":266,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_133.png":
{
	"frame": {"x":200,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_134.png":
{
	"frame": {"x":134,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_135.png":
{
	"frame": {"x":68,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_136.png":
{
	"frame": {"x":2,"y":40,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_137.png":
{
	"frame": {"x":926,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_138.png":
{
	"frame": {"x":860,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_139.png":
{
	"frame": {"x":794,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_140.png":
{
	"frame": {"x":728,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_141.png":
{
	"frame": {"x":662,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_142.png":
{
	"frame": {"x":596,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_143.png":
{
	"frame": {"x":530,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_144.png":
{
	"frame": {"x":464,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_145.png":
{
	"frame": {"x":398,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_146.png":
{
	"frame": {"x":332,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_147.png":
{
	"frame": {"x":266,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_148.png":
{
	"frame": {"x":200,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_149.png":
{
	"frame": {"x":134,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_150.png":
{
	"frame": {"x":68,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
},
"gq_151.png":
{
	"frame": {"x":2,"y":2,"w":64,"h":36},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":36},
	"sourceSize": {"w":64,"h":36}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "gq.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:45dae307de5016b5d2c5809eb9be8196$"
}
}
