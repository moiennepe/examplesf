{"frames": {

"bk_xytb.png":
{
	"frame": {"x":2,"y":222,"w":122,"h":114},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":122,"h":114},
	"sourceSize": {"w":122,"h":114}
},
"icon_jbfcxzk.png":
{
	"frame": {"x":2,"y":453,"w":52,"h":52},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":52,"h":52},
	"sourceSize": {"w":52,"h":52}
},
"kfkh_rldb001.png":
{
	"frame": {"x":126,"y":222,"w":101,"h":104},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":101,"h":104},
	"sourceSize": {"w":101,"h":104}
},
"kfkh_rldb002.png":
{
	"frame": {"x":81,"y":338,"w":101,"h":104},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":101,"h":104},
	"sourceSize": {"w":101,"h":104}
},
"kfkh_rldbxzk.png":
{
	"frame": {"x":205,"y":414,"w":74,"h":76},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":74,"h":76},
	"sourceSize": {"w":76,"h":76}
},
"num_bg.png":
{
	"frame": {"x":56,"y":453,"w":147,"h":47},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":147,"h":47},
	"sourceSize": {"w":147,"h":47}
},
"pttq_zcan01a.png":
{
	"frame": {"x":206,"y":2,"w":202,"h":96},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":202,"h":96},
	"sourceSize": {"w":202,"h":96}
},
"pttq_zcan01b.png":
{
	"frame": {"x":2,"y":2,"w":202,"h":96},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":202,"h":96},
	"sourceSize": {"w":202,"h":96}
},
"syxhd_dltou001.png":
{
	"frame": {"x":133,"y":100,"w":30,"h":98},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":30,"h":98},
	"sourceSize": {"w":30,"h":98}
},
"syxhd_dltou002.png":
{
	"frame": {"x":184,"y":328,"w":35,"h":84},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":35,"h":84},
	"sourceSize": {"w":35,"h":84}
},
"sz001d.png":
{
	"frame": {"x":359,"y":169,"w":63,"h":67},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":23,"y":23,"w":63,"h":67},
	"sourceSize": {"w":111,"h":111}
},
"sz002d.png":
{
	"frame": {"x":346,"y":100,"w":63,"h":67},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":23,"y":23,"w":63,"h":67},
	"sourceSize": {"w":111,"h":111}
},
"sz003d.png":
{
	"frame": {"x":286,"y":328,"w":63,"h":67},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":23,"y":23,"w":63,"h":67},
	"sourceSize": {"w":111,"h":111}
},
"sz004d.png":
{
	"frame": {"x":294,"y":196,"w":63,"h":67},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":23,"y":23,"w":63,"h":67},
	"sourceSize": {"w":111,"h":111}
},
"sz005d.png":
{
	"frame": {"x":229,"y":196,"w":63,"h":67},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":23,"y":23,"w":63,"h":67},
	"sourceSize": {"w":111,"h":111}
},
"sz006d.png":
{
	"frame": {"x":281,"y":397,"w":63,"h":67},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":23,"y":23,"w":63,"h":67},
	"sourceSize": {"w":111,"h":111}
},
"xzhsz001.png":
{
	"frame": {"x":411,"y":100,"w":61,"h":67},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":25,"y":21,"w":61,"h":67},
	"sourceSize": {"w":111,"h":111}
},
"xzhsz002.png":
{
	"frame": {"x":229,"y":265,"w":67,"h":61},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":20,"y":24,"w":67,"h":61},
	"sourceSize": {"w":111,"h":111}
},
"xzhsz003.png":
{
	"frame": {"x":298,"y":265,"w":63,"h":61},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":26,"y":26,"w":63,"h":61},
	"sourceSize": {"w":111,"h":111}
},
"xzhsz004.png":
{
	"frame": {"x":424,"y":236,"w":61,"h":65},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":25,"y":25,"w":61,"h":65},
	"sourceSize": {"w":111,"h":111}
},
"xzhsz005.png":
{
	"frame": {"x":221,"y":328,"w":63,"h":67},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":21,"y":22,"w":63,"h":67},
	"sourceSize": {"w":111,"h":111}
},
"xzhsz006.png":
{
	"frame": {"x":424,"y":169,"w":65,"h":65},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":24,"y":21,"w":65,"h":65},
	"sourceSize": {"w":111,"h":111}
},
"xzhsz007.png":
{
	"frame": {"x":363,"y":238,"w":51,"h":61},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":31,"y":25,"w":51,"h":61},
	"sourceSize": {"w":111,"h":111}
},
"yhdzp_jichidb001.png":
{
	"frame": {"x":2,"y":100,"w":129,"h":120},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":129,"h":120},
	"sourceSize": {"w":129,"h":120}
},
"yhdzp_jichidb003.png":
{
	"frame": {"x":410,"y":2,"w":96,"h":96},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":96,"h":96},
	"sourceSize": {"w":96,"h":96}
},
"yhdzp_zshsan001.png":
{
	"frame": {"x":165,"y":100,"w":179,"h":94},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":179,"h":94},
	"sourceSize": {"w":179,"h":94}
},
"zz_xytb.png":
{
	"frame": {"x":2,"y":338,"w":77,"h":113},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":77,"h":113},
	"sourceSize": {"w":77,"h":113}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "huo_dong_gui.png",
	"format": "RGBA8888",
	"size": {"w":512,"h":512},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:eff73a005dc8c1fcaaef8bd9d273cbe8$"
}
}
