{"frames": {

"icon_bwqd.png":
{
	"frame": {"x":902,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_bwqh.png":
{
	"frame": {"x":812,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_bwsx.png":
{
	"frame": {"x":722,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_clbx_jin01.png":
{
	"frame": {"x":632,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_ysz.png":
{
	"frame": {"x":632,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_ysz01.png":
{
	"frame": {"x":632,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_gmhf_01.png":
{
	"frame": {"x":632,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_gmhz_01.png":
{
	"frame": {"x":902,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_gmjz_01.png":
{
	"frame": {"x":812,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_gmxl_01.png":
{
	"frame": {"x":722,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_gw_dlzs.png":
{
	"frame": {"x":632,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_gw_jb.png":
{
	"frame": {"x":542,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_gw_zs.png":
{
	"frame": {"x":542,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_arfkq.png":
{
	"frame": {"x":542,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_bymj.png":
{
	"frame": {"x":542,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_hbcqu.png":
{
	"frame": {"x":542,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_hbsj.png":
{
	"frame": {"x":902,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_hbsjie.png":
{
	"frame": {"x":812,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_hsi.png":
{
	"frame": {"x":722,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_jjc.png":
{
	"frame": {"x":632,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_jnsji.png":
{
	"frame": {"x":542,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_jskc.png":
{
	"frame": {"x":452,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_jyfb.png":
{
	"frame": {"x":452,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_jyjyfb.png":
{
	"frame": {"x":452,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_mwtf.png":
{
	"frame": {"x":452,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_ptfb.png":
{
	"frame": {"x":452,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_qhzb.png":
{
	"frame": {"x":452,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_sjxj.png":
{
	"frame": {"x":902,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_xhmj.png":
{
	"frame": {"x":812,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_yjhb.png":
{
	"frame": {"x":722,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_yk.png":
{
	"frame": {"x":632,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_zk.png":
{
	"frame": {"x":542,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_jjds_qd.png":
{
	"frame": {"x":452,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_jt_boss.png":
{
	"frame": {"x":362,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_jt_qd.png":
{
	"frame": {"x":362,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_jybw001.png":
{
	"frame": {"x":362,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_jybw002.png":
{
	"frame": {"x":362,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_jybw003.png":
{
	"frame": {"x":362,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mghf_01.png":
{
	"frame": {"x":362,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mghz_01.png":
{
	"frame": {"x":362,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mgjz_01.png":
{
	"frame": {"x":902,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mgxl_01.png":
{
	"frame": {"x":812,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mj1.png":
{
	"frame": {"x":722,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mj10.png":
{
	"frame": {"x":632,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mj2.png":
{
	"frame": {"x":542,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mj3.png":
{
	"frame": {"x":452,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mj4.png":
{
	"frame": {"x":362,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mj5.png":
{
	"frame": {"x":272,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mj6.png":
{
	"frame": {"x":272,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mj7.png":
{
	"frame": {"x":272,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mj8.png":
{
	"frame": {"x":272,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mj9.png":
{
	"frame": {"x":272,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mrhf_01.png":
{
	"frame": {"x":272,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mrhz_01.png":
{
	"frame": {"x":272,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mrjz_01.png":
{
	"frame": {"x":272,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_mrxl_01.png":
{
	"frame": {"x":902,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_yshf_01.png":
{
	"frame": {"x":812,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_yshz_01.png":
{
	"frame": {"x":722,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_ysjz_01.png":
{
	"frame": {"x":632,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_ysxl_01.png":
{
	"frame": {"x":542,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_zbbx_jin.png":
{
	"frame": {"x":452,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_zbbx_jin01.png":
{
	"frame": {"x":362,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jltb_icon.png":
{
	"frame": {"x":272,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jlystb_icon.png":
{
	"frame": {"x":182,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_001.png":
{
	"frame": {"x":182,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_002.png":
{
	"frame": {"x":182,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_003.png":
{
	"frame": {"x":182,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_004.png":
{
	"frame": {"x":182,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_005.png":
{
	"frame": {"x":182,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_006.png":
{
	"frame": {"x":182,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_007.png":
{
	"frame": {"x":182,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_008.png":
{
	"frame": {"x":182,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_009.png":
{
	"frame": {"x":902,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_010.png":
{
	"frame": {"x":812,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_011.png":
{
	"frame": {"x":722,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_012.png":
{
	"frame": {"x":632,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_013.png":
{
	"frame": {"x":542,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_014.png":
{
	"frame": {"x":452,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_015.png":
{
	"frame": {"x":362,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jthzn_016.png":
{
	"frame": {"x":272,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_bwjl001.png":
{
	"frame": {"x":182,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_chongwu001.png":
{
	"frame": {"x":92,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_d3hbkq001.png":
{
	"frame": {"x":92,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_dbqb001.png":
{
	"frame": {"x":92,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_hbjn001.png":
{
	"frame": {"x":92,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_hbpy001.png":
{
	"frame": {"x":92,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_hbyh001.png":
{
	"frame": {"x":92,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_hy001.png":
{
	"frame": {"x":92,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_jtsd001.png":
{
	"frame": {"x":92,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_jzby.png":
{
	"frame": {"x":92,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_kqhy001.png":
{
	"frame": {"x":92,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_kqlt001.png":
{
	"frame": {"x":902,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_lr001.png":
{
	"frame": {"x":812,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_rcrw001.png":
{
	"frame": {"x":722,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_sc001.png":
{
	"frame": {"x":632,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_shdian001.png":
{
	"frame": {"x":542,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_xy001.png":
{
	"frame": {"x":452,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_xytz.png":
{
	"frame": {"x":362,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_zbgz001.png":
{
	"frame": {"x":272,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_zbjl001.png":
{
	"frame": {"x":182,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"small_zbsx001.png":
{
	"frame": {"x":92,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"tphd_i001.png":
{
	"frame": {"x":2,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"tphd_i002.png":
{
	"frame": {"x":2,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"tphd_i003.png":
{
	"frame": {"x":2,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"tphd_i004.png":
{
	"frame": {"x":2,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"tphd_i005.png":
{
	"frame": {"x":2,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"tphd_i006.png":
{
	"frame": {"x":2,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"tphd_i007.png":
{
	"frame": {"x":2,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"tphd_i008.png":
{
	"frame": {"x":2,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"tphd_i009.png":
{
	"frame": {"x":2,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xyj_bztb001.png":
{
	"frame": {"x":2,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xyj_bztb002.png":
{
	"frame": {"x":2,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "huodong_icon.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:3f1d0f761b99ba9cce921d0ee5933514$"
}
}
