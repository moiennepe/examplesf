{"frames": {

"bwspzz_hs001.png":
{
	"frame": {"x":722,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_hs002.png":
{
	"frame": {"x":722,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_hs003.png":
{
	"frame": {"x":722,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_hs004.png":
{
	"frame": {"x":902,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_hs005.png":
{
	"frame": {"x":812,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_hs006.png":
{
	"frame": {"x":722,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_js001.png":
{
	"frame": {"x":632,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_js002.png":
{
	"frame": {"x":632,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_js003.png":
{
	"frame": {"x":632,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_js004.png":
{
	"frame": {"x":632,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_js005.png":
{
	"frame": {"x":902,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_ls001.png":
{
	"frame": {"x":812,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_ls002.png":
{
	"frame": {"x":902,"y":808,"w":88,"h":80},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":80},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_ls003.png":
{
	"frame": {"x":722,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_zs001.png":
{
	"frame": {"x":812,"y":900,"w":84,"h":88},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":84,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_zs002.png":
{
	"frame": {"x":812,"y":722,"w":88,"h":86},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":86},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_zs003.png":
{
	"frame": {"x":812,"y":810,"w":86,"h":88},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":86,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"bwspzz_zs004.png":
{
	"frame": {"x":902,"y":722,"w":88,"h":84},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":4,"w":88,"h":84},
	"sourceSize": {"w":88,"h":88}
},
"hshbzld_icon.png":
{
	"frame": {"x":632,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"hsty_lihe001.png":
{
	"frame": {"x":542,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"hsty_lihe002.png":
{
	"frame": {"x":542,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"hszbzld_icon.png":
{
	"frame": {"x":542,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip000.png":
{
	"frame": {"x":542,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip001.png":
{
	"frame": {"x":542,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip002.png":
{
	"frame": {"x":902,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip003.png":
{
	"frame": {"x":812,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip004.png":
{
	"frame": {"x":722,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip005.png":
{
	"frame": {"x":632,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip006.png":
{
	"frame": {"x":542,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip007.png":
{
	"frame": {"x":452,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip008.png":
{
	"frame": {"x":452,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip009.png":
{
	"frame": {"x":452,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip010.png":
{
	"frame": {"x":452,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip011.png":
{
	"frame": {"x":452,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ibx_vip012.png":
{
	"frame": {"x":452,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_bwbx_zi.png":
{
	"frame": {"x":902,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_chjl.png":
{
	"frame": {"x":812,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_clbx_bai.png":
{
	"frame": {"x":722,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_clbx_ho.png":
{
	"frame": {"x":632,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_clbx_jin.png":
{
	"frame": {"x":542,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_clbx_lan.png":
{
	"frame": {"x":452,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_clbx_lv.png":
{
	"frame": {"x":362,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_clbx_zi.png":
{
	"frame": {"x":362,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_csgdlb_h.png":
{
	"frame": {"x":362,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_csgdlb_s.png":
{
	"frame": {"x":362,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_csgdlb_z.png":
{
	"frame": {"x":362,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_cshyl.png":
{
	"frame": {"x":362,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_cssjcw_z.png":
{
	"frame": {"x":362,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_cssjxz_h.png":
{
	"frame": {"x":902,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_cssjxz_hb.png":
{
	"frame": {"x":812,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_cssjxz_s.png":
{
	"frame": {"x":722,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_cssjxz_z.png":
{
	"frame": {"x":632,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_cszscw.png":
{
	"frame": {"x":542,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_cszssz.png":
{
	"frame": {"x":452,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_hj_lan.png":
{
	"frame": {"x":362,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_hssj001.png":
{
	"frame": {"x":272,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_lsj003a.png":
{
	"frame": {"x":272,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_nlka.png":
{
	"frame": {"x":272,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_nlkb.png":
{
	"frame": {"x":272,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_nlkc.png":
{
	"frame": {"x":272,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_nlkd.png":
{
	"frame": {"x":272,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_ysd.png":
{
	"frame": {"x":272,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_ysl.png":
{
	"frame": {"x":272,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_dj_ysz.png":
{
	"frame": {"x":902,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_fl.png":
{
	"frame": {"x":812,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_jx.png":
{
	"frame": {"x":722,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_jys.png":
{
	"frame": {"x":632,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_sm.png":
{
	"frame": {"x":542,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hd_zz.png":
{
	"frame": {"x":452,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hongbao01.png":
{
	"frame": {"x":362,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hongbao02.png":
{
	"frame": {"x":272,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hongbao03.png":
{
	"frame": {"x":182,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hsgdlb_h.png":
{
	"frame": {"x":182,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hsgdlb_s.png":
{
	"frame": {"x":182,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hsgdlb_z.png":
{
	"frame": {"x":182,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hshbsz.png":
{
	"frame": {"x":182,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hssjxz_h.png":
{
	"frame": {"x":182,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hssjxz_hb.png":
{
	"frame": {"x":182,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hssjxz_s.png":
{
	"frame": {"x":182,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hssjxz_z.png":
{
	"frame": {"x":182,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_hszbzl.png":
{
	"frame": {"x":902,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_jljsd001.png":
{
	"frame": {"x":812,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_jrhd_c.png":
{
	"frame": {"x":722,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_jrhd_z.png":
{
	"frame": {"x":632,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_jxzl.png":
{
	"frame": {"x":542,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_lssjxz_z.png":
{
	"frame": {"x":452,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_szfztz001.png":
{
	"frame": {"x":362,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_szjsd.png":
{
	"frame": {"x":272,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_zbbx_lv.png":
{
	"frame": {"x":182,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_zshyl.png":
{
	"frame": {"x":92,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_zssjxz_h.png":
{
	"frame": {"x":92,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_zssjxz_hb.png":
{
	"frame": {"x":92,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"icon_zssjxz_s.png":
{
	"frame": {"x":92,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jlystb_icon.png":
{
	"frame": {"x":92,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jnsjsd_001.png":
{
	"frame": {"x":92,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"mwyqkd_001.png":
{
	"frame": {"x":92,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"sp_wnjin.png":
{
	"frame": {"x":92,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"ts_tx125.png":
{
	"frame": {"x":92,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_a_fy.png":
{
	"frame": {"x":92,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_a_gj.png":
{
	"frame": {"x":902,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_a_yf.png":
{
	"frame": {"x":812,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_b_fy.png":
{
	"frame": {"x":722,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_b_gj.png":
{
	"frame": {"x":632,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_b_yf.png":
{
	"frame": {"x":542,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_c_fy.png":
{
	"frame": {"x":452,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_c_gj.png":
{
	"frame": {"x":362,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_c_yf.png":
{
	"frame": {"x":272,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_d_fy.png":
{
	"frame": {"x":182,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_d_gj.png":
{
	"frame": {"x":92,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_d_yf.png":
{
	"frame": {"x":2,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_jhys00.png":
{
	"frame": {"x":2,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_s_fy.png":
{
	"frame": {"x":2,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_s_gj.png":
{
	"frame": {"x":2,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_s_yf.png":
{
	"frame": {"x":2,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_w_fy.png":
{
	"frame": {"x":2,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_w_gj.png":
{
	"frame": {"x":2,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xz_w_yf.png":
{
	"frame": {"x":2,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zr_ghsp.png":
{
	"frame": {"x":2,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zr_ghsphs.png":
{
	"frame": {"x":2,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zr_gys.png":
{
	"frame": {"x":2,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "item_icon.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:11287d390c25a446202e58943b0d7cba$"
}
}
