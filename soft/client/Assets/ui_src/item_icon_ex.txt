{"frames": {

"cwsl_001.png":
{
	"frame": {"x":362,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"cwsl_002.png":
{
	"frame": {"x":272,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"cwsl_003.png":
{
	"frame": {"x":182,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"cwsl_004.png":
{
	"frame": {"x":92,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"cwtb_xjs.png":
{
	"frame": {"x":92,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jkz_001.png":
{
	"frame": {"x":92,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jkz_002.png":
{
	"frame": {"x":92,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jkz_003.png":
{
	"frame": {"x":92,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jkz_004.png":
{
	"frame": {"x":92,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jkz_005.png":
{
	"frame": {"x":92,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jkz_006.png":
{
	"frame": {"x":92,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"jkz_007.png":
{
	"frame": {"x":92,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"sp_gojzm.png":
{
	"frame": {"x":92,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"syz_001.png":
{
	"frame": {"x":902,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"syz_002.png":
{
	"frame": {"x":812,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"syz_003.png":
{
	"frame": {"x":722,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"syz_004.png":
{
	"frame": {"x":632,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"syz_005.png":
{
	"frame": {"x":542,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"syz_006.png":
{
	"frame": {"x":452,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"syz_007.png":
{
	"frame": {"x":362,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xtz_001.png":
{
	"frame": {"x":272,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xtz_002.png":
{
	"frame": {"x":182,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xtz_003.png":
{
	"frame": {"x":92,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xtz_004.png":
{
	"frame": {"x":2,"y":902,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xtz_005.png":
{
	"frame": {"x":2,"y":812,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xtz_006.png":
{
	"frame": {"x":2,"y":722,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"xtz_007.png":
{
	"frame": {"x":2,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zdz_001.png":
{
	"frame": {"x":2,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zdz_002.png":
{
	"frame": {"x":2,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zdz_003.png":
{
	"frame": {"x":2,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zdz_004.png":
{
	"frame": {"x":2,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zdz_005.png":
{
	"frame": {"x":2,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zdz_006.png":
{
	"frame": {"x":2,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zdz_007.png":
{
	"frame": {"x":2,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "item_icon_ex.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:baf4d55eeac9263cd00e2e4a34f58223$"
}
}
