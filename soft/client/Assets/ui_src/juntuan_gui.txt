{"frames": {

"bz_ironframe03_b01.png":
{
	"frame": {"x":674,"y":145,"w":133,"h":46},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":3,"w":133,"h":46},
	"sourceSize": {"w":133,"h":49}
},
"bz_ironframe03_b01a.png":
{
	"frame": {"x":766,"y":85,"w":110,"h":46},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":3,"w":110,"h":46},
	"sourceSize": {"w":110,"h":49}
},
"dwtb_001.png":
{
	"frame": {"x":878,"y":85,"w":136,"h":136},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":7,"w":136,"h":136},
	"sourceSize": {"w":141,"h":145}
},
"dwtb_001t.png":
{
	"frame": {"x":646,"y":360,"w":216,"h":43},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":216,"h":43},
	"sourceSize": {"w":216,"h":43}
},
"dwtb_001x.png":
{
	"frame": {"x":169,"y":1002,"w":21,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":21,"h":20},
	"sourceSize": {"w":21,"h":20}
},
"dwtb_002.png":
{
	"frame": {"x":314,"y":300,"w":136,"h":141},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":2,"w":136,"h":141},
	"sourceSize": {"w":141,"h":145}
},
"dwtb_002t.png":
{
	"frame": {"x":646,"y":405,"w":216,"h":43},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":216,"h":43},
	"sourceSize": {"w":216,"h":43}
},
"dwtb_002x.png":
{
	"frame": {"x":192,"y":1002,"w":21,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":21,"h":20},
	"sourceSize": {"w":21,"h":20}
},
"dwtb_003.png":
{
	"frame": {"x":452,"y":299,"w":136,"h":141},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":2,"w":136,"h":141},
	"sourceSize": {"w":141,"h":145}
},
"dwtb_003t.png":
{
	"frame": {"x":646,"y":450,"w":216,"h":43},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":216,"h":43},
	"sourceSize": {"w":216,"h":43}
},
"dwtb_003x.png":
{
	"frame": {"x":215,"y":1002,"w":21,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":21,"h":20},
	"sourceSize": {"w":21,"h":20}
},
"dwtb_004.png":
{
	"frame": {"x":628,"y":2,"w":136,"h":141},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":2,"w":136,"h":141},
	"sourceSize": {"w":141,"h":145}
},
"dwtb_004t.png":
{
	"frame": {"x":646,"y":495,"w":216,"h":43},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":216,"h":43},
	"sourceSize": {"w":216,"h":43}
},
"dwtb_004x.png":
{
	"frame": {"x":238,"y":1002,"w":21,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":21,"h":20},
	"sourceSize": {"w":21,"h":20}
},
"dwtb_005.png":
{
	"frame": {"x":348,"y":2,"w":140,"h":143},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":140,"h":143},
	"sourceSize": {"w":141,"h":145}
},
"dwtb_005t.png":
{
	"frame": {"x":646,"y":540,"w":216,"h":43},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":216,"h":43},
	"sourceSize": {"w":216,"h":43}
},
"dwtb_005x.png":
{
	"frame": {"x":261,"y":1002,"w":21,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":21,"h":20},
	"sourceSize": {"w":21,"h":20}
},
"dwtb_006.png":
{
	"frame": {"x":876,"y":223,"w":136,"h":136},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":7,"w":136,"h":136},
	"sourceSize": {"w":141,"h":145}
},
"dwtb_006t.png":
{
	"frame": {"x":543,"y":732,"w":216,"h":43},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":216,"h":43},
	"sourceSize": {"w":216,"h":43}
},
"dwtb_006x.png":
{
	"frame": {"x":284,"y":1002,"w":21,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":21,"h":20},
	"sourceSize": {"w":21,"h":20}
},
"dwtb_007.png":
{
	"frame": {"x":490,"y":2,"w":136,"h":143},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":0,"w":136,"h":143},
	"sourceSize": {"w":141,"h":145}
},
"dwtb_007t.png":
{
	"frame": {"x":761,"y":585,"w":216,"h":43},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":216,"h":43},
	"sourceSize": {"w":216,"h":43}
},
"dwtb_007x.png":
{
	"frame": {"x":388,"y":798,"w":21,"h":20},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":21,"h":20},
	"sourceSize": {"w":21,"h":20}
},
"goldlight_gd01.png":
{
	"frame": {"x":209,"y":207,"w":91,"h":91},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":91,"h":91},
	"sourceSize": {"w":91,"h":91}
},
"goldlight_gd02.png":
{
	"frame": {"x":302,"y":207,"w":91,"h":91},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":91,"h":91},
	"sourceSize": {"w":91,"h":91}
},
"goldlight_gd03.png":
{
	"frame": {"x":586,"y":197,"w":91,"h":91},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":91,"h":91},
	"sourceSize": {"w":91,"h":91}
},
"goldlight_gd04.png":
{
	"frame": {"x":864,"y":361,"w":91,"h":91},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":91,"h":91},
	"sourceSize": {"w":91,"h":91}
},
"goldlight_gd05.png":
{
	"frame": {"x":864,"y":454,"w":91,"h":91},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":91,"h":91},
	"sourceSize": {"w":91,"h":91}
},
"goldlight_gd06.png":
{
	"frame": {"x":543,"y":546,"w":91,"h":91},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":91,"h":91},
	"sourceSize": {"w":91,"h":91}
},
"goldlight_gd07.png":
{
	"frame": {"x":543,"y":639,"w":91,"h":91},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":91,"h":91},
	"sourceSize": {"w":91,"h":91}
},
"goldlight_gd08.png":
{
	"frame": {"x":636,"y":630,"w":91,"h":91},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":91,"h":91},
	"sourceSize": {"w":91,"h":91}
},
"icon_slbxg.png":
{
	"frame": {"x":903,"y":630,"w":91,"h":76},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":24,"w":91,"h":76},
	"sourceSize": {"w":100,"h":100}
},
"icon_slbxk.png":
{
	"frame": {"x":395,"y":197,"w":89,"h":100},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":0,"w":89,"h":100},
	"sourceSize": {"w":100,"h":100}
},
"icon_xzkg.png":
{
	"frame": {"x":486,"y":197,"w":98,"h":98},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":6,"w":98,"h":98},
	"sourceSize": {"w":110,"h":110}
},
"icon_xzkk.png":
{
	"frame": {"x":679,"y":193,"w":110,"h":110},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":110,"h":110},
	"sourceSize": {"w":110,"h":110}
},
"jlicon_cyzj001.png":
{
	"frame": {"x":809,"y":133,"w":67,"h":76},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":67,"h":76},
	"sourceSize": {"w":67,"h":76}
},
"njt_jdtdb001.png":
{
	"frame": {"x":646,"y":290,"w":31,"h":38},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":31,"h":38},
	"sourceSize": {"w":31,"h":38}
},
"njt_jdtdb002.png":
{
	"frame": {"x":109,"y":1002,"w":58,"h":20},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":3,"w":58,"h":20},
	"sourceSize": {"w":58,"h":26}
},
"njt_jdtdb003.png":
{
	"frame": {"x":236,"y":551,"w":46,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":0,"w":46,"h":26},
	"sourceSize": {"w":58,"h":26}
},
"nnssl_gkbg.png":
{
	"frame": {"x":766,"y":2,"w":251,"h":81},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":251,"h":81},
	"sourceSize": {"w":255,"h":81}
},
"nnssl_hb_b.png":
{
	"frame": {"x":307,"y":988,"w":168,"h":31},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":168,"h":31},
	"sourceSize": {"w":168,"h":31}
},
"nnssl_hb_bg.png":
{
	"frame": {"x":2,"y":2,"w":205,"h":299},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":205,"h":299},
	"sourceSize": {"w":205,"h":299}
},
"nnssl_hb_g.png":
{
	"frame": {"x":761,"y":713,"w":168,"h":31},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":168,"h":31},
	"sourceSize": {"w":168,"h":31}
},
"nnssl_hb_r.png":
{
	"frame": {"x":761,"y":746,"w":168,"h":31},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":168,"h":31},
	"sourceSize": {"w":168,"h":31}
},
"nnssl_sxbh.png":
{
	"frame": {"x":173,"y":551,"w":61,"h":26},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":61,"h":26},
	"sourceSize": {"w":61,"h":26}
},
"nnssl_xkp001a.png":
{
	"frame": {"x":510,"y":442,"w":58,"h":102},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":58,"h":102},
	"sourceSize": {"w":58,"h":102}
},
"nnssl_xkp004.png":
{
	"frame": {"x":348,"y":147,"w":161,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":161,"h":48},
	"sourceSize": {"w":161,"h":48}
},
"nnssl_xkp005.png":
{
	"frame": {"x":511,"y":147,"w":161,"h":48},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":161,"h":48},
	"sourceSize": {"w":161,"h":48}
},
"nnssl_xkp005a.png":
{
	"frame": {"x":284,"y":551,"w":125,"h":25},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":125,"h":25},
	"sourceSize": {"w":125,"h":25}
},
"nxhmj_gwxs002.png":
{
	"frame": {"x":729,"y":630,"w":172,"h":81},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":172,"h":81},
	"sourceSize": {"w":172,"h":81}
},
"small_i1001.png":
{
	"frame": {"x":791,"y":298,"w":75,"h":60},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":12,"w":75,"h":60},
	"sourceSize": {"w":85,"h":85}
},
"small_jthb.png":
{
	"frame": {"x":791,"y":211,"w":83,"h":85},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":83,"h":85},
	"sourceSize": {"w":85,"h":85}
},
"zxjt_btmdb001.png":
{
	"frame": {"x":570,"y":442,"w":68,"h":62},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":68,"h":62},
	"sourceSize": {"w":68,"h":62}
},
"zxjt_dt_001.png":
{
	"frame": {"x":109,"y":838,"w":194,"h":162},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":194,"h":162},
	"sourceSize": {"w":194,"h":162}
},
"zxjt_dt_002.png":
{
	"frame": {"x":176,"y":579,"w":210,"h":244},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":210,"h":244},
	"sourceSize": {"w":210,"h":244}
},
"zxjt_ghz_001.png":
{
	"frame": {"x":411,"y":443,"w":97,"h":129},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":97,"h":129},
	"sourceSize": {"w":97,"h":129}
},
"zxjt_ghz_002.png":
{
	"frame": {"x":173,"y":303,"w":139,"h":246},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":139,"h":246},
	"sourceSize": {"w":139,"h":246}
},
"zxjt_hd_001.png":
{
	"frame": {"x":388,"y":578,"w":153,"h":218},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":153,"h":218},
	"sourceSize": {"w":153,"h":218}
},
"zxjt_hd_002.png":
{
	"frame": {"x":2,"y":582,"w":172,"h":254},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":172,"h":254},
	"sourceSize": {"w":172,"h":254}
},
"zxjt_jtkj_001.png":
{
	"frame": {"x":314,"y":443,"w":82,"h":104},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":82,"h":104},
	"sourceSize": {"w":82,"h":104}
},
"zxjt_jtkj_002.png":
{
	"frame": {"x":2,"y":838,"w":105,"h":171},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":105,"h":171},
	"sourceSize": {"w":105,"h":171}
},
"zxjt_mb_001.png":
{
	"frame": {"x":477,"y":798,"w":167,"h":211},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":167,"h":211},
	"sourceSize": {"w":167,"h":211}
},
"zxjt_mb_002.png":
{
	"frame": {"x":2,"y":303,"w":169,"h":277},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":169,"h":277},
	"sourceSize": {"w":169,"h":277}
},
"zxjt_sd_001.png":
{
	"frame": {"x":305,"y":825,"w":142,"h":161},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":142,"h":161},
	"sourceSize": {"w":142,"h":161}
},
"zxjt_sd_002.png":
{
	"frame": {"x":209,"y":2,"w":137,"h":203},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":137,"h":203},
	"sourceSize": {"w":137,"h":203}
}},
"meta": {
	"app": "https://www.codeandweb.com/texturepacker",
	"version": "1.0",
	"image": "juntuan_gui.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:f163dbb69e7cff7b98de25eaf012ed57:e810bb7ffe6836ce53c1f9b8a62e9e6e:f1333aa9550340f3a660d00bfd80c895$"
}
}
