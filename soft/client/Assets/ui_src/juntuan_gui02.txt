{"frames": {

"gaoda_01.png":
{
	"frame": {"x":2,"y":2,"w":263,"h":235},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":12,"w":263,"h":235},
	"sourceSize": {"w":263,"h":247}
},
"gaoda_02.png":
{
	"frame": {"x":502,"y":2,"w":224,"h":193},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":224,"h":193},
	"sourceSize": {"w":224,"h":193}
},
"gaoda_03.png":
{
	"frame": {"x":728,"y":2,"w":167,"h":117},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":167,"h":117},
	"sourceSize": {"w":167,"h":117}
},
"gaoda_04.png":
{
	"frame": {"x":267,"y":70,"w":200,"h":132},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":200,"h":132},
	"sourceSize": {"w":200,"h":132}
},
"kfjtz_jt01.png":
{
	"frame": {"x":469,"y":70,"w":22,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":22,"h":27},
	"sourceSize": {"w":22,"h":27}
},
"kfjtz_jt02.png":
{
	"frame": {"x":267,"y":2,"w":233,"h":66},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":233,"h":66},
	"sourceSize": {"w":233,"h":66}
},
"kfjtz_jt03.png":
{
	"frame": {"x":633,"y":197,"w":72,"h":39},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":72,"h":39},
	"sourceSize": {"w":72,"h":39}
},
"mjdzt_a.png":
{
	"frame": {"x":702,"y":296,"w":156,"h":173},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":7,"w":156,"h":173},
	"sourceSize": {"w":164,"h":187}
},
"mjdzt_b.png":
{
	"frame": {"x":728,"y":121,"w":164,"h":173},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":7,"w":164,"h":173},
	"sourceSize": {"w":164,"h":187}
},
"slbzt_a.png":
{
	"frame": {"x":267,"y":204,"w":150,"h":182},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":6,"w":150,"h":182},
	"sourceSize": {"w":164,"h":194}
},
"slbzt_b.png":
{
	"frame": {"x":860,"y":349,"w":152,"h":136},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":53,"w":152,"h":136},
	"sourceSize": {"w":164,"h":194}
},
"syxhd_dltou001.png":
{
	"frame": {"x":419,"y":308,"w":55,"h":55},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":5,"w":55,"h":55},
	"sourceSize": {"w":67,"h":67}
},
"syxhd_dltou002.png":
{
	"frame": {"x":633,"y":238,"w":67,"h":67},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":67,"h":67},
	"sourceSize": {"w":67,"h":67}
},
"syxhd_dltou003.png":
{
	"frame": {"x":633,"y":307,"w":55,"h":55},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":5,"w":55,"h":55},
	"sourceSize": {"w":67,"h":67}
},
"yjdzt_a.png":
{
	"frame": {"x":894,"y":235,"w":98,"h":112},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":7,"w":98,"h":112},
	"sourceSize": {"w":132,"h":124}
},
"yjdzt_b.png":
{
	"frame": {"x":897,"y":2,"w":122,"h":102},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":17,"w":122,"h":102},
	"sourceSize": {"w":132,"h":124}
},
"zjdzt_a.png":
{
	"frame": {"x":897,"y":106,"w":104,"h":127},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":14,"y":8,"w":104,"h":127},
	"sourceSize": {"w":178,"h":153}
},
"zjdzt_b.png":
{
	"frame": {"x":469,"y":197,"w":162,"h":109},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":8,"y":38,"w":162,"h":109},
	"sourceSize": {"w":178,"h":153}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "juntuan_gui02.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:8974084401c6c7b4694a88f024e34a88$"
}
}
