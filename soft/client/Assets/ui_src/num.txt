{"frames": {

"button_nu01.png":
{
	"frame": {"x":444,"y":296,"w":26,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":32},
	"sourceSize": {"w":26,"h":32}
},
"button_nu010.png":
{
	"frame": {"x":416,"y":269,"w":26,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":32},
	"sourceSize": {"w":26,"h":32}
},
"button_nu011.png":
{
	"frame": {"x":356,"y":61,"w":24,"h":32},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":24,"h":32},
	"sourceSize": {"w":26,"h":32}
},
"button_nu012.png":
{
	"frame": {"x":144,"y":252,"w":26,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":30},
	"sourceSize": {"w":26,"h":32}
},
"button_nu013.png":
{
	"frame": {"x":116,"y":252,"w":26,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":30},
	"sourceSize": {"w":26,"h":32}
},
"button_nu014.png":
{
	"frame": {"x":172,"y":244,"w":26,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":30},
	"sourceSize": {"w":26,"h":32}
},
"button_nu015.png":
{
	"frame": {"x":472,"y":322,"w":26,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":30},
	"sourceSize": {"w":26,"h":32}
},
"button_nu02.png":
{
	"frame": {"x":254,"y":244,"w":22,"h":32},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":22,"h":32},
	"sourceSize": {"w":26,"h":32}
},
"button_nu03.png":
{
	"frame": {"x":388,"y":269,"w":26,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":32},
	"sourceSize": {"w":26,"h":32}
},
"button_nu04.png":
{
	"frame": {"x":360,"y":246,"w":26,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":32},
	"sourceSize": {"w":26,"h":32}
},
"button_nu05.png":
{
	"frame": {"x":332,"y":243,"w":26,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":32},
	"sourceSize": {"w":26,"h":32}
},
"button_nu06.png":
{
	"frame": {"x":484,"y":288,"w":26,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":32},
	"sourceSize": {"w":26,"h":32}
},
"button_nu07.png":
{
	"frame": {"x":484,"y":254,"w":26,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":32},
	"sourceSize": {"w":26,"h":32}
},
"button_nu08.png":
{
	"frame": {"x":152,"y":176,"w":26,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":32},
	"sourceSize": {"w":26,"h":32}
},
"button_nu09.png":
{
	"frame": {"x":152,"y":142,"w":26,"h":32},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":26,"h":32},
	"sourceSize": {"w":26,"h":32}
},
"fight_b00.png":
{
	"frame": {"x":180,"y":202,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b01.png":
{
	"frame": {"x":248,"y":202,"w":28,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":6,"y":0,"w":28,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b02.png":
{
	"frame": {"x":300,"y":145,"w":38,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":38,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b03.png":
{
	"frame": {"x":446,"y":254,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b04.png":
{
	"frame": {"x":408,"y":227,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b05.png":
{
	"frame": {"x":370,"y":204,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b06.png":
{
	"frame": {"x":332,"y":201,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b07.png":
{
	"frame": {"x":40,"y":261,"w":34,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":0,"w":34,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b08.png":
{
	"frame": {"x":260,"y":118,"w":38,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":38,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b09.png":
{
	"frame": {"x":294,"y":187,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b10.png":
{
	"frame": {"x":466,"y":128,"w":40,"h":40},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":40,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b11.png":
{
	"frame": {"x":381,"y":120,"w":40,"h":40},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":40,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b12.png":
{
	"frame": {"x":314,"y":103,"w":40,"h":40},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":40,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_b13.png":
{
	"frame": {"x":220,"y":118,"w":38,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":38,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g00.png":
{
	"frame": {"x":256,"y":160,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g01.png":
{
	"frame": {"x":218,"y":202,"w":28,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":7,"y":0,"w":28,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g02.png":
{
	"frame": {"x":2,"y":179,"w":38,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":38,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g03.png":
{
	"frame": {"x":218,"y":160,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g04.png":
{
	"frame": {"x":2,"y":221,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g05.png":
{
	"frame": {"x":42,"y":219,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g06.png":
{
	"frame": {"x":98,"y":184,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g07.png":
{
	"frame": {"x":80,"y":226,"w":34,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":4,"y":0,"w":34,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g08.png":
{
	"frame": {"x":58,"y":177,"w":38,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":38,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g09.png":
{
	"frame": {"x":180,"y":160,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g10.png":
{
	"frame": {"x":424,"y":101,"w":40,"h":40},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":40,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g11.png":
{
	"frame": {"x":382,"y":78,"w":40,"h":40},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":40,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g12.png":
{
	"frame": {"x":314,"y":61,"w":40,"h":40},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":40,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_g13.png":
{
	"frame": {"x":112,"y":142,"w":38,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":38,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r00.png":
{
	"frame": {"x":454,"y":212,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r01.png":
{
	"frame": {"x":2,"y":263,"w":28,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":0,"w":28,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r02.png":
{
	"frame": {"x":180,"y":118,"w":38,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":38,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r03.png":
{
	"frame": {"x":416,"y":185,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r04.png":
{
	"frame": {"x":378,"y":162,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r05.png":
{
	"frame": {"x":340,"y":159,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r06.png":
{
	"frame": {"x":474,"y":44,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r07.png":
{
	"frame": {"x":136,"y":210,"w":34,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":0,"w":34,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r08.png":
{
	"frame": {"x":463,"y":170,"w":38,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":0,"w":38,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r09.png":
{
	"frame": {"x":474,"y":2,"w":36,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":36,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r10.png":
{
	"frame": {"x":466,"y":86,"w":40,"h":40},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":40,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r11.png":
{
	"frame": {"x":424,"y":59,"w":40,"h":40},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":40,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r12.png":
{
	"frame": {"x":382,"y":36,"w":40,"h":40},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":40,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"fight_r13.png":
{
	"frame": {"x":423,"y":143,"w":38,"h":40},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":0,"w":38,"h":40},
	"sourceSize": {"w":40,"h":40}
},
"hits_00.png":
{
	"frame": {"x":2,"y":120,"w":54,"h":57},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":12,"y":2,"w":54,"h":57},
	"sourceSize": {"w":78,"h":59}
},
"hits_01.png":
{
	"frame": {"x":432,"y":2,"w":40,"h":55},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":21,"y":3,"w":40,"h":55},
	"sourceSize": {"w":78,"h":59}
},
"hits_02.png":
{
	"frame": {"x":202,"y":2,"w":60,"h":55},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":8,"y":3,"w":60,"h":55},
	"sourceSize": {"w":78,"h":59}
},
"hits_03.png":
{
	"frame": {"x":256,"y":59,"w":56,"h":57},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":11,"y":2,"w":56,"h":57},
	"sourceSize": {"w":78,"h":59}
},
"hits_04.png":
{
	"frame": {"x":122,"y":85,"w":56,"h":55},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":10,"y":3,"w":56,"h":55},
	"sourceSize": {"w":78,"h":59}
},
"hits_05.png":
{
	"frame": {"x":264,"y":2,"w":58,"h":55},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":10,"y":4,"w":58,"h":55},
	"sourceSize": {"w":78,"h":59}
},
"hits_06.png":
{
	"frame": {"x":198,"y":59,"w":56,"h":57},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":13,"y":2,"w":56,"h":57},
	"sourceSize": {"w":78,"h":59}
},
"hits_07.png":
{
	"frame": {"x":58,"y":120,"w":52,"h":55},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":15,"y":3,"w":52,"h":55},
	"sourceSize": {"w":78,"h":59}
},
"hits_08.png":
{
	"frame": {"x":62,"y":61,"w":58,"h":57},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":10,"y":2,"w":58,"h":57},
	"sourceSize": {"w":78,"h":59}
},
"hits_09.png":
{
	"frame": {"x":2,"y":61,"w":58,"h":57},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":10,"y":2,"w":58,"h":57},
	"sourceSize": {"w":78,"h":59}
},
"hits_10.png":
{
	"frame": {"x":72,"y":2,"w":64,"h":57},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":9,"y":1,"w":64,"h":57},
	"sourceSize": {"w":78,"h":59}
},
"hits_11.png":
{
	"frame": {"x":2,"y":2,"w":68,"h":57},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":1,"w":68,"h":57},
	"sourceSize": {"w":78,"h":59}
},
"hits_12.png":
{
	"frame": {"x":138,"y":26,"w":58,"h":57},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":9,"y":1,"w":58,"h":57},
	"sourceSize": {"w":78,"h":59}
},
"hits_13.png":
{
	"frame": {"x":324,"y":2,"w":56,"h":57},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":14,"y":1,"w":56,"h":57},
	"sourceSize": {"w":78,"h":59}
},
"spzdl_xtb-fb.png":
{
	"frame": {"x":138,"y":2,"w":62,"h":22},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":6,"w":62,"h":22},
	"sourceSize": {"w":70,"h":30}
},
"vip_0.png":
{
	"frame": {"x":274,"y":317,"w":21,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":3,"w":21,"h":30},
	"sourceSize": {"w":27,"h":34}
},
"vip_1.png":
{
	"frame": {"x":353,"y":280,"w":19,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":3,"w":19,"h":30},
	"sourceSize": {"w":27,"h":34}
},
"vip_2.png":
{
	"frame": {"x":303,"y":261,"w":23,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":3,"w":23,"h":30},
	"sourceSize": {"w":27,"h":34}
},
"vip_3.png":
{
	"frame": {"x":278,"y":257,"w":23,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":3,"w":23,"h":30},
	"sourceSize": {"w":27,"h":34}
},
"vip_4.png":
{
	"frame": {"x":199,"y":278,"w":23,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":3,"w":23,"h":30},
	"sourceSize": {"w":27,"h":34}
},
"vip_5.png":
{
	"frame": {"x":307,"y":229,"w":23,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":3,"w":23,"h":30},
	"sourceSize": {"w":27,"h":34}
},
"vip_6.png":
{
	"frame": {"x":302,"y":293,"w":21,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":3,"w":21,"h":30},
	"sourceSize": {"w":27,"h":34}
},
"vip_7.png":
{
	"frame": {"x":254,"y":278,"w":21,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":3,"w":21,"h":30},
	"sourceSize": {"w":27,"h":34}
},
"vip_8.png":
{
	"frame": {"x":356,"y":127,"w":23,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":3,"w":23,"h":30},
	"sourceSize": {"w":27,"h":34}
},
"vip_9.png":
{
	"frame": {"x":356,"y":95,"w":23,"h":30},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":3,"w":23,"h":30},
	"sourceSize": {"w":27,"h":34}
},
"vip_anwz.png":
{
	"frame": {"x":382,"y":2,"w":48,"h":32},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":2,"w":48,"h":32},
	"sourceSize": {"w":52,"h":34}
},
"vip_b.png":
{
	"frame": {"x":227,"y":272,"w":25,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":3,"w":25,"h":26},
	"sourceSize": {"w":27,"h":34}
},
"vip_i.png":
{
	"frame": {"x":492,"y":212,"w":17,"h":32},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":5,"y":2,"w":17,"h":32},
	"sourceSize": {"w":27,"h":34}
},
"vip_k.png":
{
	"frame": {"x":227,"y":244,"w":25,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":3,"w":25,"h":26},
	"sourceSize": {"w":27,"h":34}
},
"vip_m.png":
{
	"frame": {"x":278,"y":229,"w":27,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":3,"w":27,"h":26},
	"sourceSize": {"w":27,"h":34}
},
"vip_p.png":
{
	"frame": {"x":172,"y":276,"w":25,"h":32},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":2,"w":25,"h":32},
	"sourceSize": {"w":27,"h":34}
},
"vip_s.png":
{
	"frame": {"x":200,"y":244,"w":25,"h":32},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":2,"w":25,"h":32},
	"sourceSize": {"w":27,"h":34}
},
"vip_t.png":
{
	"frame": {"x":277,"y":289,"w":23,"h":26},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":3,"y":3,"w":23,"h":26},
	"sourceSize": {"w":27,"h":34}
},
"vip_v.png":
{
	"frame": {"x":76,"y":268,"w":25,"h":32},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":2,"y":2,"w":25,"h":32},
	"sourceSize": {"w":27,"h":34}
},
"xzjm_zsjysz00.png":
{
	"frame": {"x":318,"y":325,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz01.png":
{
	"frame": {"x":180,"y":85,"w":15,"h":22},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":1,"y":0,"w":15,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz02.png":
{
	"frame": {"x":297,"y":325,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz03.png":
{
	"frame": {"x":346,"y":312,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz04.png":
{
	"frame": {"x":395,"y":303,"w":19,"h":20},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":0,"y":1,"w":19,"h":20},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz05.png":
{
	"frame": {"x":374,"y":303,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz06.png":
{
	"frame": {"x":145,"y":284,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz07.png":
{
	"frame": {"x":124,"y":284,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz08.png":
{
	"frame": {"x":2,"y":305,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz09.png":
{
	"frame": {"x":53,"y":303,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz10.png":
{
	"frame": {"x":32,"y":303,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz11.png":
{
	"frame": {"x":76,"y":302,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz12.png":
{
	"frame": {"x":103,"y":284,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjysz13.png":
{
	"frame": {"x":325,"y":301,"w":19,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":22},
	"sourceSize": {"w":19,"h":22}
},
"xzjm_zsjyszwan.png":
{
	"frame": {"x":328,"y":277,"w":23,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":23,"h":22},
	"sourceSize": {"w":23,"h":22}
},
"xzjm_zsjyszwan_cn.png":
{
	"frame": {"x":249,"y":310,"w":23,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":23,"h":22},
	"sourceSize": {"w":23,"h":22}
},
"xzjm_zsjyszwan_cnj.png":
{
	"frame": {"x":224,"y":300,"w":23,"h":22},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":23,"h":22},
	"sourceSize": {"w":23,"h":22}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "num.png",
	"format": "RGBA8888",
	"size": {"w":512,"h":512},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:52ead614896cf9bdf40d113fd3040e9d$"
}
}
