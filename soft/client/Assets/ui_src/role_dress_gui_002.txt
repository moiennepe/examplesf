{"frames": {

"ts_hbsz_002d.png":
{
	"frame": {"x":313,"y":472,"w":97,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":31,"y":4,"w":97,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_003d.png":
{
	"frame": {"x":107,"y":2,"w":99,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":28,"y":6,"w":99,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_004d.png":
{
	"frame": {"x":857,"y":316,"w":95,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":30,"y":4,"w":95,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_005d.png":
{
	"frame": {"x":479,"y":2,"w":85,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":34,"y":6,"w":85,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_006d.png":
{
	"frame": {"x":2,"y":470,"w":83,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":37,"y":4,"w":83,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_007d.png":
{
	"frame": {"x":295,"y":2,"w":87,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":34,"y":6,"w":87,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_008d.png":
{
	"frame": {"x":717,"y":158,"w":93,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":32,"y":6,"w":93,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_009d.png":
{
	"frame": {"x":208,"y":2,"w":85,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":36,"y":6,"w":85,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_010d.png":
{
	"frame": {"x":610,"y":472,"w":89,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":34,"y":2,"w":89,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_011d.png":
{
	"frame": {"x":360,"y":314,"w":87,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":34,"y":4,"w":87,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_012d.png":
{
	"frame": {"x":659,"y":314,"w":85,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":34,"y":4,"w":85,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_013d.png":
{
	"frame": {"x":895,"y":474,"w":93,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":31,"y":2,"w":93,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_014d.png":
{
	"frame": {"x":872,"y":634,"w":97,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":28,"y":2,"w":97,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_015d.png":
{
	"frame": {"x":184,"y":314,"w":87,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":34,"y":4,"w":87,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_016d.png":
{
	"frame": {"x":746,"y":314,"w":109,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":20,"y":4,"w":109,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_017d.png":
{
	"frame": {"x":701,"y":472,"w":93,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":32,"y":2,"w":93,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_018c.png":
{
	"frame": {"x":87,"y":472,"w":97,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":32,"y":4,"w":97,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_018d.png":
{
	"frame": {"x":273,"y":314,"w":85,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":35,"y":4,"w":85,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_019a.png":
{
	"frame": {"x":449,"y":314,"w":109,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":24,"y":4,"w":109,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_019b.png":
{
	"frame": {"x":121,"y":790,"w":119,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":26,"y":2,"w":119,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_019c.png":
{
	"frame": {"x":549,"y":792,"w":99,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":27,"y":2,"w":99,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_020a.png":
{
	"frame": {"x":87,"y":314,"w":95,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":31,"y":4,"w":95,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_020b.png":
{
	"frame": {"x":329,"y":630,"w":103,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":29,"y":2,"w":103,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_020c.png":
{
	"frame": {"x":224,"y":630,"w":103,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":28,"y":2,"w":103,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_020d.png":
{
	"frame": {"x":434,"y":630,"w":85,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":36,"y":2,"w":85,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_021a.png":
{
	"frame": {"x":566,"y":2,"w":143,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":8,"y":6,"w":143,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_021b.png":
{
	"frame": {"x":588,"y":158,"w":127,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":13,"y":6,"w":127,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_021c.png":
{
	"frame": {"x":840,"y":2,"w":127,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":13,"y":6,"w":127,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_021d.png":
{
	"frame": {"x":2,"y":312,"w":83,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":37,"y":4,"w":83,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_022a.png":
{
	"frame": {"x":560,"y":314,"w":97,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":26,"y":4,"w":97,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_022b.png":
{
	"frame": {"x":279,"y":158,"w":89,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":34,"y":6,"w":89,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_022c.png":
{
	"frame": {"x":93,"y":158,"w":89,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":33,"y":6,"w":89,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_022d.png":
{
	"frame": {"x":184,"y":158,"w":93,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":30,"y":6,"w":93,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_023a.png":
{
	"frame": {"x":186,"y":472,"w":125,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":14,"y":4,"w":125,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_023b.png":
{
	"frame": {"x":642,"y":632,"w":119,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":16,"y":2,"w":119,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_023c.png":
{
	"frame": {"x":521,"y":632,"w":119,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":16,"y":2,"w":119,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_023d.png":
{
	"frame": {"x":2,"y":156,"w":89,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":34,"y":6,"w":89,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_024a.png":
{
	"frame": {"x":242,"y":790,"w":107,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":25,"y":2,"w":107,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_024b.png":
{
	"frame": {"x":370,"y":158,"w":111,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":25,"y":6,"w":111,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_024c.png":
{
	"frame": {"x":2,"y":2,"w":103,"h":152},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":26,"y":8,"w":103,"h":152},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_024d.png":
{
	"frame": {"x":483,"y":158,"w":103,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":27,"y":6,"w":103,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_025a.png":
{
	"frame": {"x":711,"y":2,"w":127,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":12,"y":6,"w":127,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_025b.png":
{
	"frame": {"x":796,"y":474,"w":97,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":31,"y":2,"w":97,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_025c.png":
{
	"frame": {"x":911,"y":158,"w":97,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":32,"y":4,"w":97,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_025d.png":
{
	"frame": {"x":412,"y":472,"w":91,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":34,"y":4,"w":91,"h":156},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_026a.png":
{
	"frame": {"x":2,"y":790,"w":117,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":20,"y":2,"w":117,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_026b.png":
{
	"frame": {"x":2,"y":630,"w":119,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":14,"y":2,"w":119,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_026c.png":
{
	"frame": {"x":751,"y":794,"w":119,"h":160},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":19,"y":0,"w":119,"h":160},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_026d.png":
{
	"frame": {"x":763,"y":634,"w":107,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":24,"y":2,"w":107,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_027a.png":
{
	"frame": {"x":650,"y":792,"w":99,"h":160},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":27,"y":0,"w":99,"h":160},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_027b.png":
{
	"frame": {"x":351,"y":790,"w":99,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":28,"y":2,"w":99,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_027c.png":
{
	"frame": {"x":452,"y":792,"w":95,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":29,"y":2,"w":95,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_027d.png":
{
	"frame": {"x":384,"y":2,"w":93,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":31,"y":6,"w":93,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_028a.png":
{
	"frame": {"x":812,"y":158,"w":97,"h":154},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":29,"y":6,"w":97,"h":154},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_028b.png":
{
	"frame": {"x":872,"y":794,"w":137,"h":160},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":15,"y":0,"w":137,"h":160},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_028c.png":
{
	"frame": {"x":123,"y":630,"w":99,"h":158},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":28,"y":2,"w":99,"h":158},
	"sourceSize": {"w":155,"h":160}
},
"ts_hbsz_028d.png":
{
	"frame": {"x":505,"y":472,"w":103,"h":156},
	"rotated": false,
	"trimmed": true,
	"spriteSourceSize": {"x":24,"y":4,"w":103,"h":156},
	"sourceSize": {"w":155,"h":160}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "role_dress_gui_002.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:3ff24b7f78616623f5cf8c1c2577204a$"
}
}
