{"frames": {

"bdjn_bwaj.png":
{
	"frame": {"x":596,"y":860,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_byms.png":
{
	"frame": {"x":596,"y":794,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_dalet.png":
{
	"frame": {"x":596,"y":728,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_dbcn.png":
{
	"frame": {"x":596,"y":662,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_fzjj.png":
{
	"frame": {"x":554,"y":596,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_glzg.png":
{
	"frame": {"x":554,"y":530,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_hhzw.png":
{
	"frame": {"x":554,"y":464,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_hlsp.png":
{
	"frame": {"x":554,"y":398,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_jdsan.png":
{
	"frame": {"x":554,"y":332,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_jfcz.png":
{
	"frame": {"x":554,"y":266,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_jjdf.png":
{
	"frame": {"x":926,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_jjzq.png":
{
	"frame": {"x":860,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_jjzs.png":
{
	"frame": {"x":794,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_jzgj.png":
{
	"frame": {"x":728,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_kjsl.png":
{
	"frame": {"x":662,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_msky.png":
{
	"frame": {"x":596,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_mvza.png":
{
	"frame": {"x":530,"y":926,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_qlzj.png":
{
	"frame": {"x":530,"y":860,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_sjld.png":
{
	"frame": {"x":530,"y":794,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_tsxf.png":
{
	"frame": {"x":530,"y":728,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_tszg.png":
{
	"frame": {"x":530,"y":662,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_wmyc.png":
{
	"frame": {"x":488,"y":596,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_wmzj.png":
{
	"frame": {"x":488,"y":530,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_wxjj.png":
{
	"frame": {"x":488,"y":464,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_xhsg.png":
{
	"frame": {"x":488,"y":398,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_xsls.png":
{
	"frame": {"x":488,"y":332,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"bdjn_yzwl.png":
{
	"frame": {"x":488,"y":266,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_bdzs000.png":
{
	"frame": {"x":488,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_bjgh000.png":
{
	"frame": {"x":926,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_cjwq000.png":
{
	"frame": {"x":860,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_dlhj000.png":
{
	"frame": {"x":794,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_fhcs000.png":
{
	"frame": {"x":728,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_fsys000.png":
{
	"frame": {"x":662,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_fykz000.png":
{
	"frame": {"x":596,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_fyyz000.png":
{
	"frame": {"x":530,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_gdzt000.png":
{
	"frame": {"x":464,"y":926,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_gjgh000.png":
{
	"frame": {"x":464,"y":860,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_gjyz000.png":
{
	"frame": {"x":464,"y":794,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_gjzt000.png":
{
	"frame": {"x":464,"y":728,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_gjzz000.png":
{
	"frame": {"x":464,"y":662,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_hdjt000.png":
{
	"frame": {"x":422,"y":596,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_jfgj000.png":
{
	"frame": {"x":422,"y":530,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_jzgj000.png":
{
	"frame": {"x":422,"y":464,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_ldxj000.png":
{
	"frame": {"x":422,"y":398,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_mfgh000.png":
{
	"frame": {"x":422,"y":332,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_mfhj000.png":
{
	"frame": {"x":422,"y":266,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_mfkz000.png":
{
	"frame": {"x":422,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_mfms000.png":
{
	"frame": {"x":422,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_mfss000.png":
{
	"frame": {"x":926,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_mfzt000.png":
{
	"frame": {"x":860,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_mfzz000.png":
{
	"frame": {"x":794,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_mkzb000.png":
{
	"frame": {"x":728,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_mmzt000.png":
{
	"frame": {"x":662,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_nygd000.png":
{
	"frame": {"x":596,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_nygj000.png":
{
	"frame": {"x":530,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_nywm000.png":
{
	"frame": {"x":464,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_rdcj000.png":
{
	"frame": {"x":398,"y":926,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_sbzt000.png":
{
	"frame": {"x":398,"y":860,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_smzf000.png":
{
	"frame": {"x":398,"y":794,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_ssjs000.png":
{
	"frame": {"x":398,"y":728,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_sszd000.png":
{
	"frame": {"x":398,"y":662,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_swfx000.png":
{
	"frame": {"x":356,"y":596,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_swzz000.png":
{
	"frame": {"x":356,"y":530,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_tyjn000.png":
{
	"frame": {"x":356,"y":464,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_wfgh000.png":
{
	"frame": {"x":356,"y":398,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_wfzt000.png":
{
	"frame": {"x":356,"y":332,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_wfzz000.png":
{
	"frame": {"x":356,"y":266,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_wkzb000.png":
{
	"frame": {"x":356,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_wlkz000.png":
{
	"frame": {"x":356,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_wlms000.png":
{
	"frame": {"x":356,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_wlss000.png":
{
	"frame": {"x":926,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_xggh000.png":
{
	"frame": {"x":860,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_xgzz000.png":
{
	"frame": {"x":794,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_xxgj000.png":
{
	"frame": {"x":728,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_xymy000.png":
{
	"frame": {"x":662,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icjn_yqzq000.png":
{
	"frame": {"x":596,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icon_jjzc000.png":
{
	"frame": {"x":530,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icon_jszt000.png":
{
	"frame": {"x":464,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icon_nlzf000.png":
{
	"frame": {"x":398,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"icon_zszt000.png":
{
	"frame": {"x":332,"y":926,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jnsd_zztb001.png":
{
	"frame": {"x":332,"y":860,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtbj.png":
{
	"frame": {"x":332,"y":794,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtct.png":
{
	"frame": {"x":332,"y":728,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtgd.png":
{
	"frame": {"x":332,"y":662,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtgj.png":
{
	"frame": {"x":290,"y":596,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtjs.png":
{
	"frame": {"x":290,"y":530,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtkb.png":
{
	"frame": {"x":290,"y":464,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtmf.png":
{
	"frame": {"x":290,"y":398,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtmz.png":
{
	"frame": {"x":290,"y":332,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtsb.png":
{
	"frame": {"x":290,"y":266,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtsm.png":
{
	"frame": {"x":290,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtwf.png":
{
	"frame": {"x":290,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtxg.png":
{
	"frame": {"x":290,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"jtjn_jtzs.png":
{
	"frame": {"x":290,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_amb.png":
{
	"frame": {"x":266,"y":926,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_asnai.png":
{
	"frame": {"x":266,"y":860,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_bbl.png":
{
	"frame": {"x":266,"y":794,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_bshe.png":
{
	"frame": {"x":266,"y":728,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_bysw.png":
{
	"frame": {"x":266,"y":662,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_cc.png":
{
	"frame": {"x":224,"y":596,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_cgw.png":
{
	"frame": {"x":224,"y":530,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_chig.png":
{
	"frame": {"x":224,"y":464,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_chuy.png":
{
	"frame": {"x":224,"y":398,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_cpjj.png":
{
	"frame": {"x":224,"y":332,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_cyz.png":
{
	"frame": {"x":224,"y":266,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_dour.png":
{
	"frame": {"x":224,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_dyhdw.png":
{
	"frame": {"x":224,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_gjs.png":
{
	"frame": {"x":224,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_gm.png":
{
	"frame": {"x":224,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_gwn.png":
{
	"frame": {"x":200,"y":926,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_gybj.png":
{
	"frame": {"x":200,"y":860,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_hjsw.png":
{
	"frame": {"x":200,"y":794,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_hob.png":
{
	"frame": {"x":200,"y":728,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_hsss.png":
{
	"frame": {"x":200,"y":662,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_hv.png":
{
	"frame": {"x":158,"y":596,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_hz.png":
{
	"frame": {"x":158,"y":530,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_jfls.png":
{
	"frame": {"x":158,"y":464,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_jjjj.png":
{
	"frame": {"x":158,"y":398,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_jwyh.png":
{
	"frame": {"x":158,"y":332,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_jxnp.png":
{
	"frame": {"x":158,"y":266,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_ks.png":
{
	"frame": {"x":158,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_lxzq.png":
{
	"frame": {"x":158,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_mrx.png":
{
	"frame": {"x":158,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_myou.png":
{
	"frame": {"x":158,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_nyj.png":
{
	"frame": {"x":134,"y":926,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_paoj.png":
{
	"frame": {"x":134,"y":860,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_pjts.png":
{
	"frame": {"x":134,"y":794,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_qg.png":
{
	"frame": {"x":134,"y":728,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_qntz.png":
{
	"frame": {"x":134,"y":662,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_qxts.png":
{
	"frame": {"x":92,"y":596,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_saber.png":
{
	"frame": {"x":92,"y":530,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_szl.png":
{
	"frame": {"x":92,"y":464,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_xm.png":
{
	"frame": {"x":92,"y":398,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_xnv.png":
{
	"frame": {"x":92,"y":332,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_xsfd.png":
{
	"frame": {"x":92,"y":266,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_xyt.png":
{
	"frame": {"x":92,"y":200,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_yaom.png":
{
	"frame": {"x":92,"y":134,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_yhsv.png":
{
	"frame": {"x":92,"y":68,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_ykls.png":
{
	"frame": {"x":92,"y":2,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_ylgz.png":
{
	"frame": {"x":68,"y":920,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_ymo.png":
{
	"frame": {"x":68,"y":854,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_ytu.png":
{
	"frame": {"x":68,"y":788,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_ywjz.png":
{
	"frame": {"x":68,"y":722,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_zero.png":
{
	"frame": {"x":2,"y":920,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_zjts.png":
{
	"frame": {"x":2,"y":854,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_zm.png":
{
	"frame": {"x":2,"y":788,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zdjn_ic_zytz.png":
{
	"frame": {"x":2,"y":722,"w":64,"h":64},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":64,"h":64},
	"sourceSize": {"w":64,"h":64}
},
"zr_gh01.png":
{
	"frame": {"x":2,"y":632,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zr_gh02.png":
{
	"frame": {"x":2,"y":542,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zr_gh03.png":
{
	"frame": {"x":2,"y":452,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zr_gh04.png":
{
	"frame": {"x":2,"y":362,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zr_gh05.png":
{
	"frame": {"x":2,"y":272,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zr_gh06.png":
{
	"frame": {"x":2,"y":182,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zr_gh07.png":
{
	"frame": {"x":2,"y":92,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
},
"zr_ghsphs.png":
{
	"frame": {"x":2,"y":2,"w":88,"h":88},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":88,"h":88},
	"sourceSize": {"w":88,"h":88}
}},
"meta": {
	"app": "http://www.texturepacker.com",
	"version": "1.0",
	"image": "skill_icon.png",
	"format": "RGBA8888",
	"size": {"w":1024,"h":1024},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:4ed49983b51c934055918a85e35d58c4$"
}
}
