INT	STRING	STRING	INT	INT	INT	INT	INT	STRING	STRING
id	name	代号	翅膀id	强化	type 1 加def1属性def2点 2 双倍攻击概率+def1 3 毅力抵抗回复百分比+def1	def1	def2	描述	代号
1001	光环强攻	t_guanghuan_skill_name_1001	2001	10	1	2	150		t_guanghuan_skill_desc_1001
1002	光环强攻	t_guanghuan_skill_name_1002	2001	20	1	2	150		t_guanghuan_skill_desc_1002
1003	光环强攻	t_guanghuan_skill_name_1003	2001	30	1	2	150		t_guanghuan_skill_desc_1003
1004	光环强攻	t_guanghuan_skill_name_1004	2001	40	1	2	150		t_guanghuan_skill_desc_1004
1005	光环强攻	t_guanghuan_skill_name_1005	2001	50	1	2	150		t_guanghuan_skill_desc_1005
1006	光环强攻	t_guanghuan_skill_name_1006	2001	60	1	2	150		t_guanghuan_skill_desc_1006
1007	光环强攻	t_guanghuan_skill_name_1007	2001	70	1	2	150		t_guanghuan_skill_desc_1007
1008	光环强攻	t_guanghuan_skill_name_1008	2001	80	1	2	150		t_guanghuan_skill_desc_1008
1009	光环强攻	t_guanghuan_skill_name_1009	2001	90	1	2	150		t_guanghuan_skill_desc_1009
1010	光环强攻	t_guanghuan_skill_name_1010	2001	100	1	2	150		t_guanghuan_skill_desc_1010
2001	光环强攻	t_guanghuan_skill_name_2001	3001	10	1	2	600		t_guanghuan_skill_desc_2001
2002	光环强攻	t_guanghuan_skill_name_2002	3001	20	1	2	600		t_guanghuan_skill_desc_2002
2003	光环强攻	t_guanghuan_skill_name_2003	3001	30	1	2	600		t_guanghuan_skill_desc_2003
2004	光环强攻	t_guanghuan_skill_name_2004	3001	40	1	2	600		t_guanghuan_skill_desc_2004
2005	光环强攻	t_guanghuan_skill_name_2005	3001	50	1	2	600		t_guanghuan_skill_desc_2005
2006	光环强攻	t_guanghuan_skill_name_2006	3001	60	1	2	600		t_guanghuan_skill_desc_2006
2007	光环强攻	t_guanghuan_skill_name_2007	3001	70	1	2	600		t_guanghuan_skill_desc_2007
2008	光环强攻	t_guanghuan_skill_name_2008	3001	80	1	2	600		t_guanghuan_skill_desc_2008
2009	光环强攻	t_guanghuan_skill_name_2009	3001	90	1	2	600		t_guanghuan_skill_desc_2009
2010	光环强攻	t_guanghuan_skill_name_2010	3001	100	1	2	600		t_guanghuan_skill_desc_2010
3001	光环强命	t_guanghuan_skill_name_3001	3002	10	1	1	6000		t_guanghuan_skill_desc_3001
3002	光环强命	t_guanghuan_skill_name_3002	3002	20	1	1	6000		t_guanghuan_skill_desc_3002
3003	光环强命	t_guanghuan_skill_name_3003	3002	30	1	1	6000		t_guanghuan_skill_desc_3003
3004	光环强命	t_guanghuan_skill_name_3004	3002	40	1	1	6000		t_guanghuan_skill_desc_3004
3005	光环强命	t_guanghuan_skill_name_3005	3002	50	1	1	6000		t_guanghuan_skill_desc_3005
3006	光环强命	t_guanghuan_skill_name_3006	3002	60	1	1	6000		t_guanghuan_skill_desc_3006
3007	光环强命	t_guanghuan_skill_name_3007	3002	70	1	1	6000		t_guanghuan_skill_desc_3007
3008	光环强命	t_guanghuan_skill_name_3008	3002	80	1	1	6000		t_guanghuan_skill_desc_3008
3009	光环强命	t_guanghuan_skill_name_3009	3002	90	1	1	6000		t_guanghuan_skill_desc_3009
3010	光环强命	t_guanghuan_skill_name_3010	3002	100	1	1	6000		t_guanghuan_skill_desc_3010
4001	光环强攻	t_guanghuan_skill_name_4001	4001	10	1	2	1200		t_guanghuan_skill_desc_4001
4002	光环强攻	t_guanghuan_skill_name_4002	4001	20	1	2	1200		t_guanghuan_skill_desc_4002
4003	强化·致命一击I	t_guanghuan_skill_name_4003	4001	30	2	1		触发装备圣器技能[致命一击]的概率额外提升{{n1}}%	t_guanghuan_skill_desc_4003
4004	光环强攻	t_guanghuan_skill_name_4004	4001	40	1	2	1200		t_guanghuan_skill_desc_4004
4005	强化·致命一击II	t_guanghuan_skill_name_4005	4001	50	2	2		触发装备圣器技能[致命一击]的概率额外提升{{n1}}%	t_guanghuan_skill_desc_4005
4006	光环强攻	t_guanghuan_skill_name_4006	4001	60	1	2	1200		t_guanghuan_skill_desc_4006
4007	强化·致命一击III	t_guanghuan_skill_name_4007	4001	70	2	2		触发装备圣器技能[致命一击]的概率额外提升{{n1}}%	t_guanghuan_skill_desc_4007
4008	光环强攻	t_guanghuan_skill_name_4008	4001	80	1	2	1200		t_guanghuan_skill_desc_4008
4009	光环强攻	t_guanghuan_skill_name_4009	4001	90	1	2	1200		t_guanghuan_skill_desc_4009
4010	光环强攻	t_guanghuan_skill_name_4010	4001	100	1	2	1200		t_guanghuan_skill_desc_4010
5001	光环强命	t_guanghuan_skill_name_5001	4002	10	1	1	12000		t_guanghuan_skill_desc_5001
5002	光环强命	t_guanghuan_skill_name_5002	4002	20	1	1	12000		t_guanghuan_skill_desc_5002
5003	强化·毅力抵抗I	t_guanghuan_skill_name_5003	4002	30	3	1		装备圣器技能[毅力抵抗]回复生命比例额外提升{{n1}}%	t_guanghuan_skill_desc_5003
5004	光环强命	t_guanghuan_skill_name_5004	4002	40	1	1	12000		t_guanghuan_skill_desc_5004
5005	强化·毅力抵抗II	t_guanghuan_skill_name_5005	4002	50	3	2		装备圣器技能[毅力抵抗]回复生命比例额外提升{{n1}}%	t_guanghuan_skill_desc_5005
5006	光环强命	t_guanghuan_skill_name_5006	4002	60	1	1	12000		t_guanghuan_skill_desc_5006
5007	强化·毅力抵抗III	t_guanghuan_skill_name_5007	4002	70	3	2		装备圣器技能[毅力抵抗]回复生命比例额外提升{{n1}}%	t_guanghuan_skill_desc_5007
5008	光环强命	t_guanghuan_skill_name_5008	4002	80	1	1	12000		t_guanghuan_skill_desc_5008
5009	光环强命	t_guanghuan_skill_name_5009	4002	90	1	1	12000		t_guanghuan_skill_desc_5009
5010	光环强命	t_guanghuan_skill_name_5010	4002	100	1	1	12000		t_guanghuan_skill_desc_5010
6001	光环强攻	t_guanghuan_skill_name_6001	5001	10	1	2	2400		t_guanghuan_skill_desc_6001
6002	全体祝福	t_guanghuan_skill_name_6002	5001	20	1	38	2	所有上阵伙伴全属性+{{n1}}%	t_guanghuan_skill_desc_6002
6003	光环强攻	t_guanghuan_skill_name_6003	5001	30	1	2	2400		t_guanghuan_skill_desc_6003
6004	全体祝福	t_guanghuan_skill_name_6004	5001	40	1	38	2	所有上阵伙伴全属性+{{n1}}%	t_guanghuan_skill_desc_6004
6005	光环强攻	t_guanghuan_skill_name_6005	5001	50	1	2	2400		t_guanghuan_skill_desc_6005
6006	全体祝福	t_guanghuan_skill_name_6006	5001	60	1	38	2	所有上阵伙伴全属性+{{n1}}%	t_guanghuan_skill_desc_6006
6007	光环强攻	t_guanghuan_skill_name_6007	5001	70	1	2	2400		t_guanghuan_skill_desc_6007
6008	全体祝福	t_guanghuan_skill_name_6008	5001	80	1	38	2	所有上阵伙伴全属性+{{n1}}%	t_guanghuan_skill_desc_6008
6009	光环强攻	t_guanghuan_skill_name_6009	5001	90	1	2	2400		t_guanghuan_skill_desc_6009
6010	全体祝福	t_guanghuan_skill_name_6010	5001	100	1	38	2	所有上阵伙伴全属性+{{n1}}%	t_guanghuan_skill_desc_6010
7001	光环强命	t_guanghuan_skill_name_7001	5002	10	1	1	24000		t_guanghuan_skill_desc_7001
7002	全体祝福	t_guanghuan_skill_name_7002	5002	20	1	38	2	所有上阵伙伴全属性+{{n1}}%	t_guanghuan_skill_desc_7002
7003	光环强命	t_guanghuan_skill_name_7003	5002	30	1	1	24000		t_guanghuan_skill_desc_7003
7004	全体祝福	t_guanghuan_skill_name_7004	5002	40	1	38	2	所有上阵伙伴全属性+{{n1}}%	t_guanghuan_skill_desc_7004
7005	光环强命	t_guanghuan_skill_name_7005	5002	50	1	1	24000		t_guanghuan_skill_desc_7005
7006	全体祝福	t_guanghuan_skill_name_7006	5002	60	1	38	2	所有上阵伙伴全属性+{{n1}}%	t_guanghuan_skill_desc_7006
7007	光环强命	t_guanghuan_skill_name_7007	5002	70	1	1	24000		t_guanghuan_skill_desc_7007
7008	全体祝福	t_guanghuan_skill_name_7008	5002	80	1	38	2	所有上阵伙伴全属性+{{n1}}%	t_guanghuan_skill_desc_7008
7009	光环强命	t_guanghuan_skill_name_7009	5002	90	1	1	24000		t_guanghuan_skill_desc_7009
7010	全体祝福	t_guanghuan_skill_name_7010	5002	100	1	38	2	所有上阵伙伴全属性+{{n1}}%	t_guanghuan_skill_desc_7010
